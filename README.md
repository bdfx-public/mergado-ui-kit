Public preview is available in here: https://bdfx-public.gitlab.io/mergado-ui-kit/

To run live dev server just run

    npm start

and then go to http://localhost:3001

To build static html version of styleguide execute

    npm run build


Styleguide will be generated into /docs folder.

