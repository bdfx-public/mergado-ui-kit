const path = require("path");
var pjson = require('./package.json');

module.exports = {
    propsParser: require('react-docgen-typescript').parse,
    version: pjson.version,
    title: "MUK - Mergado UI Kit " + pjson.version,
    sections: [
    {
        name: 'Form',
        content: 'src/components/Form/Readme.md',
        components: () => ['src/components/Form/*/index.tsx'],
    },
    {
        name: 'Table',
        content: 'src/components/Table/Readme.md',
        components: (name) => ['src/components/Table/*/index.tsx']
    },

    {
        name: 'Data Table',
        content: 'src/components/DataTable/Readme.md',
        components: (name) => ['src/components/DataTable/*/index.tsx']
    },
    {
        name: 'Modals',
        components: () => ['src/components/Modal/*/index.tsx'],
    },
    {
        name: 'Components',
        components: (name) => { return ['src/components/*/index.tsx','src/components/**/index2.tsx' ] },
    },


    ],
    theme: {
        color: {
            link: 'white',
            linkHover: 'white',
            sidebarBackground: '#007b20',

        },
    },
    template: {
        favicon: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAGJMAABiTAGZSZ44AAAAB3RJTUUH4wMQCxg05EdtzgAABEpJREFUWMO9lW1olWUYx3/X8zzn7M1yEw0tw+lkrZAcvbC0MiUq+6RJKlRmhFRmFlpm9EIJvZgFIhKGKTjsS2qzSEjLMC0yaOCwIS18f4m0lbMtN8957vvfh7nY1tl2Nk/94Xy4z3Vz/X/XdV/3/Rj9VOWpFsAAbgOeQjwPHK8bWdTfVNCRKVuNP96Mlwep1IKgGpiEtN5Liwyafywt7jdA0J/N6dQF4nR6iPd+hYvjSS6Occ7N9c49F6fTiesONv53Hbim4VckFQQWvIXZwm7wLUjPxhcufBAmk2qoGJ5bgLH1J5H3YRCGizB73SBP3ZIITiPNs8C2eec5OG5kbgBG1x0l7/JCUs2tD5jZe0BxT3uFfkZ6COwHBIcrR10awKjaQ1gAElPMqAa7uu+a9J285mAcDmQcuams191RT4GRe3/CuRicxpnZyuzMAZiI9K6kxxz0OZUZb8FmCedjXDp9lXdupYvdeBfHZP1zbrp37jUXx4Ujvq3v/xFc8fU+gMFmwWozm5Nl5V0PAlJIrzrv3gkscGcmV2YHMHRnLZKSFgTLzFgChAMBuKhzkhYWV5RvbDrQQONdN/cOULz9e5yLLYqiJ81sBVB4CeYdOiXpETPb6b1omnpLZoDB276BKIFcfJ+ZrQWG5sC8Q/WSHgT2JxMBjXdP7Aow6JPd7eMoTcBsI1A2MJ/MuvhQ7ZI0FziRPyif3+9s70RUsOVLnEsjR7mZrbIcm3fSFMFyeb/gr6Z00z9wyY+2A4wws/XAvQAiQAMzyXivTGAIkAdWyesljNbU7KlEZsVITZMlhcAXgIZx8voCnR+Rq9LTJM+dtitrPaEDxmBUAnsTm3YQSWcxUSNsa8pPiedH0+5Jqm0t1ksP+tkeQeSU2FrDonVnyLfIUt688GHY3rCXa6po83kE5qsk2wCq6NHMQH0BZIobvxksOJoq2VyW18jy+/e2/73k49sJQ0MwDlEN3NAll7qeq9EZoHOwD6r28EmMeXjtIAhYMWMP9uJndwCMldgA3GqWZcoBTKkkJA5h9qi83xOEIVEQWlJimokY46s/WnxZS6tKOy5vr47ZjEmnPYPy7diQy4ITgulBGOwHNUVADKy5cUxi9eIPmyvOtviN8uo7o3qKty8yzYnBnyWDgmfeeHhwQ/2xdBx0dLvw6SoEQw3WIaaBMleXNUQvceNTwTygsfX9Wqxo8QQwy8frbaSFdH9KuiXVQEw7L4QwVmO2FGiLyAtDYr8As8e9mfV17P8iUPZxASZZIJ4ATlhgKyM8owmsXPB5xS/nK0ta4lIySr1Pfqap674UnC2KjjYML6gzKAdGRwZHWpZVzd86bMO1eSlfQy6V+UFKtSWCV2YcmX2g6M19ZrujYgwKnLFGMDfXfplkUB2K+YLWiParMDOCWQP+AvZfs8zY5bDqKB/GC14QFPwfzheLLDBYmkB1Vpso2QTMHEgrc6AtfwPhIxdO0KTzHgAAACV0RVh0ZGF0ZTpjcmVhdGUAMjAxOS0wMy0xNlQxMToyNDo1MiswMTowMNWuYGEAAAAldEVYdGRhdGU6bW9kaWZ5ADIwMTktMDMtMTZUMTE6MjQ6NTIrMDE6MDCk89jdAAAAGXRFWHRTb2Z0d2FyZQB3d3cuaW5rc2NhcGUub3Jnm+48GgAAAFd6VFh0UmF3IHByb2ZpbGUgdHlwZSBpcHRjAAB4nOPyDAhxVigoyk/LzEnlUgADIwsuYwsTIxNLkxQDEyBEgDTDZAMjs1Qgy9jUyMTMxBzEB8uASKBKLgDqFxF08kI1lQAAAABJRU5ErkJggg==",
    },
    require: [path.resolve(__dirname, 'src/styleguidist/setup.ts')],
    skipComponentsWithoutExample: true,
    styleguideDir: path.resolve(__dirname, "docs"),
    styleguideComponents: {
        Wrapper: path.join(__dirname, 'lib/styleguidist/Wrapper'),
        StyleGuideRenderer: path.join(__dirname, 'lib/styleguidist/StyleGuideRenderer'),
    },
};
