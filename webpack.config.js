const webpack = require("webpack");
const path = require("path");
const config = {
  mode: "development",
  entry: [
    './src/index.ts',
  ],
  output: {
    path: path.join(__dirname,'dist'),
    publicPath: './',
    filename: "bundle.js"
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/,
      }
	]
  },
	resolve: {
		extensions: ['.js', '.jsx', '.ts', '.tsx'],
      modules: [ path.resolve(__dirname, "src"), path.resolve(__dirname, "src/components"), 'node_modules', 'node_modules/react-styleguidist/lib/client/rsg-components/'],
    alias: {
      'color': 'colord',
    },

	},
};

config.plugins = [
]

module.exports = config;
