import dotProp from "dot-prop-immutable"


export const types = {
    INIT_FORM: "INIT_FORM",
    WARNING_FORM: "WARNING_FORM",
    UPDATE_INPUT_VALUE: "UPDATE_INPUT_VALUE",
}

export const changeField = (type, name, value) => ({
    type,
    name,
    value,
});

const createBoundType = namespace => (types.UPDATE_INPUT_VALUE + '.' + namespace);

const Reducer = (namespace, defaultState: any = {}, onStateChange: Function = null, actionHandlers: any = {}) =>
    (state = defaultState, action) => {
        const boundType = createBoundType(namespace);

        const reducer = {
            [types.INIT_FORM]: (state,action) => {
                if(namespace === action.payload.form) {
                    return action.payload.data;
                }
                return state;
            },
            [boundType]: () => {
                const fieldPathWithoutNamespace = action.name.replace(namespace + '.', '');
                const newState = dotProp.set(state, fieldPathWithoutNamespace, action.value);
                return onStateChange && onStateChange(newState) || newState;
            },

            ...actionHandlers,
        };

        return reducer[action.type]
            ? reducer[action.type](state, action)
            : state;
    };


function initForm(form: string, data, sync:number = null) {
    return {
        type: types.INIT_FORM,
        payload: {form, data, sync},
    }
}

function updateInputValue(form: string, name, value) {
    const formName = form.split('.')
    const fieldName = formName.slice(1)
    fieldName.push(name)
    return {
        type: types.UPDATE_INPUT_VALUE+"."+formName[0],
        name: fieldName.join('.'),
        form: formName[0],
        value,
    }
}


export default {
    types,
    changeField,
    createBoundType,
    initForm,
    updateInputValue,
    Reducer,
}