import {colord as Color} from "colord"

import defaultTheme, {colors as defaultColors} from "./default"

const colors = {
    ...defaultColors,
    /*colors*/
    blue: "#009ba9",
    green: "#69A120",
    red: "#EA4E4B",
    grey: "#ccc",
    yellow: "#ffec88",
    orange: "#FF9500",
    purple: "#793979",
    brown: "#97843e",

    background: "white",
    decoration: "#ddd",
    decoration_background: "#F9F9F9",

    teal: "#6e9588",
    steelblue: "#54809e",
    olive: "#97843e",

}

export const theme = {
    ...defaultTheme,
    ...colors,
    /* colors type */
    warning: colors.orange,
    error: colors.red,
    success: colors.green,
    info: colors.decoration_background,
    inactive: colors.grey,
    message: colors.yellow,
    validator: colors.teal,
    custom: colors.steelblue,
    suggestion: colors.olive,

    /* general */
    radius: "3px",
    selected_background: "#ccebee",
    hover_background: "#f2fafb",

    /* section */
    section_border: "1px solid " + colors.decoration,
    section_padding: "20px",

    /* form */
    form_label_text_size: "13px",
    form_label_text_weight: "600",

    /* input */
    input_border: "1px solid " + colors.decoration,
    input_border_active: "1px solid " + colors.blue,
    input_border_error: "1px solid " + colors.red,

    /* button */
    button_text_transform: "none",
    button_text_size: "16px",
    button_text_weight: "bold",

    /* datagrid */
    table_border_vertical: "1px solid " + Color(colors.decoration).alpha(0.1).toRgbString(),
    table_border_horizontal: "1px solid "+ Color(colors.decoration).alpha(0.5).toRgbString(),
    table_header_text_size: "11px",
    table_header_text_transform: "uppercase",
    table_cell_padding: "8px",
    table_cell_text_size: "15px",

    /* nav */
    nav_background: "transparent",
    nav_link_color: colors.blue,
    nav_link_background: "transparent",
    nav_link_background_active: "transparent",
    nav_link_border: `5px solid ${colors.blue}`

}

export default theme
