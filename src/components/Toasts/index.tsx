import * as React from "react"
import Toast from "../Toast"
import css from "../../css"

export interface Props {
    toasts?: Array<Toast|JSX.Element>
    style?: any
    className?: string
}
export interface State {
    toasts: Array<Toast|JSX.Element>
}

class Toasts extends React.PureComponent<Props, State> {

    private readonly name = "muk-toasts-container";

    constructor(props: Props) {
        super(props)

        this.state = {
            toasts: props.toasts,
        }
    }

    componentDidUpdate(prevProps) {
        if(!this.props.toasts || !prevProps.toasts) {
            return
        }
        if(this.props.toasts.length < prevProps.toasts.length) {
            const ids = this.props.toasts.map((t:any) => t.key)
            this.setState({ toasts: prevProps.toasts.map((toast:any) => {
                if(ids.indexOf(toast.key) === -1) {
                    return React.cloneElement(toast, { ...toast.props, hide: true })
                }
                return toast
            })})
            setTimeout(() => this.setState({ toasts: this.props.toasts }),1000)
        }
        if(this.props.toasts.length > prevProps.toasts.length) {
            this.setState({ toasts: this.props.toasts })
        }

    }

    public render() {
        const {className, style, children} = this.props
        return (
            <Wrapper className={`${this.name} ${className}`} style={style}>
                {this.props.toasts ? this.state.toasts : children}
            </Wrapper>
        )
    }
}

const Wrapper = css("div")({
    padding: "5px",
    position: "fixed",
    zIndex: 10000000,
    top: "5px",
    right: "5px",
    minWidth: "400px",

    " .muk-toast__wrapper": {
        opacity: 0.9,
        minWidth: "300px",
        animation: 'slidein 0.3s  ease-in-out',
        transition: "transform 0.5s  ease-in-out",
        maxHeight: "100px",
    },
    " .muk-toast__wrapper.removed": {
        maxHeight: "0px !important",
    },

    " .muk-toast__wrapper.ended": {
        transform: "translate3d(100vw, 0,0)",
        transition: "transform 0.5s ease-in-out",
    },

    " .muk-toast__wrapper:hover": {
        opacity: 1,
    },

    "@keyframes slidein": {
        "0%": {
            transform: "translate3d(100vw, 0,0)",
        },
        "100%": {
            transform: "translate3d(0, 0,0)",
        },
    },

    " .muk-toast__content": {
        fontSize: "0.85rem",
    },
})

export default Toasts
