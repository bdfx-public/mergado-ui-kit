Toasts container, is  position: fixed, right: 5px. It can be override by style prop.s


    initialState = { toasts: [] };
    <Section>
        <Button onClick={e => setState(s => { const toasts = [...s.toasts]; toasts.push(Math.round(Math.random() * 10) % 4); return ({ toasts, })})}>Add toast</Button>
        {" "}
        <Button onClick={e => setState(s => {  const toasts = [...s.toasts]; toasts.shift(); return ({ toasts, })})}>Remove first toast</Button>
        <div style={{position: "fixed", top: "5px", right: "5px", zIndex: 1000000, }}>
        <Toasts toasts={state.toasts.map(
            (o,i) => {
                if(o === 0) {
                    return ( <Toast text={`Toast ${o} `} id={i} onClick={o => console.log('hello')} timeout={30000} type="material" icon={<Icon type="mergado" />} />)
                } else if(o === 1) {
                    return ( <Toast text={`Toast ${o} `}  id={i} timeout={30000} type="info" icon={<Icon type="mergado" />} />)
                } else if(o === 2) {
                    return ( <Toast text={`Toast ${o} `}  id={i}  timeout={30000} type="error" icon={<Icon type="mergado" />} />)
                } else if(o === 3) {
                    return ( <Toast text={`Toast ${o} `}  id={i}  timeout={30000} type="success" icon={<Icon type="mergado" />} />)
                }
            }
        )} />
        </div>
    </Section>

