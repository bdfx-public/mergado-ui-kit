import * as React from "react"
import css from "../../css"
import Div from "../Div"
import {animation as pulseAnimation} from "../Animations/Pulse"
import {animation as rotateAnimation} from "../Animations/Rotate"
import Theme from "../Theme"

export interface Props {
    type?: "default" | "dashed" | "dotted" | "mergado" | "bubbles"
    /** Maximum dimension (width or height) */
    size?: number
    color?: string
    speed?: number
    style?: any
    className?: string
}
export interface State {
}

class Spinner extends React.PureComponent<Props, State> {

    private readonly name = "muk-spinner";

    public static defaultProps: Props = {
        type: "default",
        size: 30,
        color: Theme.decoration,
        style: {},
        speed: 1,
    }

    public render() {

        const { size, type, color, speed,className,...others } = this.props

        const containerStyle: any = type === "bubbles" ? {height: "50px"} :
                                {  width: `${size}px`, height: `${size}px` }
        return (
            <Div {...containerStyle} display={"inline-block"} overflow={"hidden"}
                className={`${this.name} ${this.name}--${this.props.type} ${className || ""}`}
                {...others}
                >
                <Wrapper {...this.props} className={`${this.name}__wrapper`}>
                    <div className="m-bubble" />
                    <div className="m-bubble second" />
                    <div className="m-bubble third" />
                </Wrapper>
            </Div>
        )
    }
}

const mergadoColors = {
    left: "#7fba2c",
    bottom: "#007b20",
    right: "#00a9b8",
    top: "transparent",
}

const Wrapper = css("div")({

}, (props: any) => {
    let type = {}
    let color = {}
    if(props.type === "mergado") {
        type = {
            borderWidth: (props.size / 2) + "px",
            boxSizing: "border-box",
            borderStyle: "solid",
            borderColor: `${mergadoColors.top} ${mergadoColors.left} ${mergadoColors.right} ${mergadoColors.bottom}`,
            position: "relative",
            ".muk-spinner__content": {
                maxWidth: 0,
                maxHeight: 0,
                overflow: "hidden",
            },
            ...pulseAnimation,
            animation: `pulse 10s infinite linear`
        }
    } else if(props.type === "bubbles") {
        type = {
            "@keyframes bouncing-loader": {
                "to": {
                    opacity: 0.1,
                    transform: "translate3d(0, -10px, 0)",
                }
            },
            width: "auto",
            border: "none !important",
            display: "flex",
            justifyContent: "center",
            willChange: "transform",
            " .m-bubble": {
                width: "10px",
                height: "10px",
                margin: "32px 1px",
                background: props.color || "#5778F3",
                borderRadius: "50%",
                animation: "bouncing-loader 0.6s infinite alternate",
            },
            " .second": {
                animationDelay: "0.2s",
            },
            " .third": {
                animationDelay: "0.4s",
            },
        }
    } else {
        type =  {
            "@keyframes donut-spin": {
                "0%": {
                    transform: "rotate(0deg)",
                },
                "100%": {
                    transform: "rotate(360deg)",
                }
            },
            display: "inline-block",
            border: "4px solid rgba(0, 0, 0, 0.1)",
            borderWidth: props.size <= 20 ? "3px" : "4px",
            borderLeftColor: props.color || "#5778F3",
            borderRadius: "50%",
            willChange: "transform",
            animation: "donut-spin 1.2s linear infinite",
        }
    }

    return {
        width: props.size + "px",
        height: props.size + "px",
        fontSize: props.size + "px",
        borderRadius: "100%",
        ...type,
        ...color,
        ...props.style,
    }
})

export default Spinner
