import * as React from "react"
import css from "../../css"
import Button from "../Button"
import Div from "../Div"

interface Props {
    color: string
    size: string
    actions: any[]
}

interface State {
    showActions: boolean
}


class ActionButton extends React.PureComponent<Props, State> {

    state = {
        showActions: false
    }

    toggleShow = () => this.setState(s => ({showActions: !s.showActions}))
    hide = () => { setTimeout(() => this.setState({showActions: false}), 100) }

    public render() {
        const { actions, color, size } = this.props
        const { showActions } = this.state
        return (
            <Style className="actions-wrapper">
                <Button color={color} onBlur={this.hide} size={size} icon={"caret-down"} onMouseDown={this.toggleShow} />
                {showActions &&
                <Actions className="actions">
                    {actions.map(a => <li className="action-item" title={a.title ?? undefined} key={a.key || a.label} onMouseDown={a.onClick}>{a.children || a.label}</li>)}
                </Actions>
                }
            </Style>
        )
    }
}

const Actions = css("ul")({
    position: "absolute",
    textAlign: "left",
    right: 0,
    listStyleType: "none",
    boxShadow: "1px 1px 10px rgba(0,0,0,0.3)",
    display: "block",
    padding: 0,
    margin: 0,
    zIndex:10,
    " li": {
        cursor: "pointer",
        padding: "8px 10px",
        display: "block",
        minWidth: "200px",
        background: "white",
        borderBottom: "1px solid #eee",
    },
    " li:hover": {
        backgroundColor: "#fafafa"
    },

})

const Style = css("div")({
    display: "inline-block",
    position: "relative",
    ".muk-button": {
        borderLeft:"1px solid rgba(0,0,0,0.1)",
        paddingLeft: "8px",
        paddingRight: "8px",
        borderRadius: "0 5px 5px 0",
        marginLeft: "-5px",
    },
})


export default ActionButton
