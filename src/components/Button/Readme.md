Test

    <div>
        Actions:<br/>
        <Button actions={[
                        {children: <strong>Hello</strong>, key: "Hello", onClick: (e) => { e.stopPropagation(); e.preventDefault(); console.log('hello'); return false;} },
                        {children: <em>"World"</em>, key: "World", onClick: () => console.log('world') },
        ]} color={"green"} onClickFn={console.log}>
            <Icon type="exclamation-triangle" text={"testing_strategy"}/>
        </Button>
        <br/>
        Children icon with text prop:<br/>
        <Button simple={true} color={"blue"} onClickFn={console.log}>
            <Icon type="exclamation-triangle" text={"testing_strategy"}/>
        </Button>
        <br/>

        <strong>Recommended use:</strong><br/>
        Prop icon and children text:<br/>
        <Button color="green" icon="check">Checked</Button>{" "}
        <Button color="red" icon={<Icon type="close" size="20" />}>Closed</Button>
        <br/>
        <br/>
        <strong>Allowed combinations of props:</strong><br/>
        Prop icon with fragment:<br/>
        <Button color="green" icon={<><Icon type="chevron-right-light">I have icon</Icon></>} />{" "}
        <br/>
        Prop icon and prop label:<br/>
        <Button label={"Cancel"}
                        icon={<Icon type="close" />}
                        type={"void"}
                        color={"gray"}
            />{" "}
        <br/>
        Children icon and prop label:<br/>
        <Button label={"Cancel"}
                type={"void"}
                color={"blue"}
        ><Icon type="close" /></Button>
        <br/>
        Without icon with string children:<br/>
        <Button onClick={() => alert('Hello')}>Button</Button>
        <br/>
        With icon prop with text prop:<br/>
        <Button color="green" icon={<Icon type="check" color="blue" text="I have icon" />} />{" "}
        <br/>
        Empty:<br/>
        <Button />

    </div>

Simple button

    <div>
        <Button onClick={() => alert('Hello')}>Button</Button> <Button
         type="href">AHREF</Button> <Button
         type="void">Void</Button> <Button
         type="submit">Submit!</Button>
    </div>

Buttons with icons

    <div>
        <Button color="green" icon={<Icon type="check" color="blue" text="I have icon" />} />{" "}
        <Button color="warning">I don't</Button>{" "}
        <Button color="red" icon={"close"} >I have bigger</Button>
        <br/><br/>
        <Button color="green" icon={<Icon type="chevron-right-light">I have icon</Icon>} />{" "}
        <Button color="warning" icon={<Icon type="chevron-left-light" text="I have icon" />} ></Button>{" "}
        <Button color="red" icon={<Icon type="chevron-down-light" text="I have icon" />} />
        <br/><br/>
        <Button color="green" size={"small"} icon={<Icon type="check" />} label="Small" />{" "}
        <Button color="red" size={"smaller"} icon={"close"} label="Smaller" />
    </div>

Button colors

    <Div style={{" .muk-button": { marginRight: "5px"}}}>
    primary:<br/>
        <Button color="blue" icon={<Icon type="chevron-right-light" />}>Blue</Button>
        <Button color="grey" icon={<Icon type="chevron-right-light" />}>Grey</Button>
        <Button color="green" icon={<Icon type="chevron-right-light" />}>Green</Button>
        <Button color="red" icon={<Icon type="chevron-right-light" />}>Red</Button>
        <Button color="orange" icon={<Icon type="chevron-right-light" />}>Orange</Button>
        <Button color="yellow" icon={<Icon type="chevron-right-light" />}>Yellow</Button>
        <Button color="decoration" icon={<Icon type="chevron-right-light" />}>Decoration</Button>
        <Button color="nocolor" icon={<Icon type="chevron-right-light" />}>Nocolor</Button>
        <br/>
        <br/>
    secondary:<br/>
        <Button color="blue" secondary={true} icon={<Icon type="chevron-right-light" />}>Blue</Button>
        <Button color="grey" secondary={true} icon={<Icon type="chevron-right-light" />}>Grey</Button>
        <Button color="green" secondary={true} icon={<Icon type="chevron-right-light" />}>Green</Button>
        <Button color="red" secondary={true} icon={<Icon type="chevron-right-light" />}>Red</Button>
        <Button color="orange" secondary={true} icon={<Icon type="chevron-right-light" />}>Orange</Button>
        <Button color="yellow" secondary={true} icon={<Icon type="chevron-right-light" />}>Yellow</Button>
        <Button color="decoration" secondary={true} icon={<Icon type="chevron-right-light" />}>Decoration</Button>
        <Button color="nocolor" secondary={true} icon={<Icon type="chevron-right-light" />}>Nocolor</Button>
    <br/>
    <br/>
    simple:<br/>
        <Button color="blue" simple={true} icon={<Icon type="chevron-right-light" />}>Blue</Button>
        <Button color="grey" simple={true} icon={<Icon type="chevron-right-light" />}>Grey</Button>
        <Button color="green" simple={true} icon={<Icon type="chevron-right-light" />}>Green</Button>
        <Button color="red" simple={true} icon={<Icon type="chevron-right-light" />}>Red</Button>
        <Button color="orange" simple={true} icon={<Icon type="chevron-right-light" />}>Orange</Button>
        <Button color="yellow" simple={true} icon={<Icon type="chevron-right-light" />}>Yellow</Button>
        <Button color="decoration" simple={true} icon={<Icon type="chevron-right-light" />}>Decoration</Button>
    <br/>
    <br/>
    squared:<br/>
        <Button rounded={false} icon={<Icon type="chevron-right-light" text="Blue" />} color="blue">Blue</Button>
        <Button rounded={false} icon={<Icon type="chevron-right-light" />} color="grey">Grey</Button>
        <Button rounded={false} icon={<Icon type="chevron-right-light" />} color="green">Green</Button>
        <Button rounded={false} icon={<Icon type="chevron-right-light" />} color="red">Red</Button>
        <Button rounded={false} icon={<Icon type="chevron-right-light" />} color="orange">Orange</Button>
        <Button rounded={false} icon={<Icon type="chevron-right-light" />} color="yellow">Yellow</Button>
        <Button rounded={false} icon={<Icon type="chevron-right-light" />} color="decoration">Decoration</Button>
        <Button rounded={false} icon={<Icon type="chevron-right-light" />} color="nocolor">Nocolor</Button>
        <br/>
        <br/>
    </Div>

Button state

    <div>
    <Button disabled={true}>Disabled</Button> <Button type="submit" error="Error message">Button with error</Button>
    </div>

Button sizes

    <div>
        <Button>Default</Button> <Button size="small">Small</Button> <Button size="smaller">Smaller</Button> <Button size="tiny">Tiny</Button>

    </div>

Icon button

    <div>
        <h3>Big icons</h3>
        <Button icon={<Icon type="reglendo" size="25" />} />
        <Button icon={<Icon type="search" size="25" />} color="nocolor" />
        <Button icon={<Icon type="pencil" size="25" />} color="nocolor" />
        <Button icon={<Icon type="play" size="25"/>} color="nocolor" />
        <Button icon={<Icon type="pause" size="25"/>} color="nocolor" />
        <Button icon={<Icon type="clipboard" size="25" />} color="nocolor" />
        <Button icon={<Icon type="trash" size="25" />} color="nocolor" />

        <h4>Small icons</h4>
        <Button icon={<Icon type="reglendo" size="20" />} size="small" />
        <Button icon={<Icon type="search" size="20" />} color="nocolor" size="small" />
        <Button icon={<Icon type="pencil" size="20" />} color="nocolor" size="small" />
        <Button icon={<Icon type="play" size="20" />} color="nocolor" size="small" />
        <Button icon={<Icon type="pause" size="20" />} color="nocolor" size="small" />
        <Button icon={<Icon type="clipboard" size="20" />} color="nocolor" size="small" />
        <Button icon={<Icon type="trash" size="20" />} color="nocolor" size="small" />

        <h5>Tiny icons</h5>
        <Button icon={<Icon type="reglendo"/>} size="tiny" />
        <Button icon={<Icon type="search" />} color="nocolor" size="tiny" />
        <Button icon={<Icon type="pencil" />} color="nocolor" size="tiny" />
        <Button icon={<Icon type="play" />} color="nocolor" size="tiny" />
        <Button icon={<Icon type="pause" />} color="nocolor" size="tiny" />
        <Button icon={<Icon type="clipboard" />} color="nocolor" size="tiny" />
        <Button icon={<Icon type="trash" />} color="nocolor" size="tiny" />
    </div>