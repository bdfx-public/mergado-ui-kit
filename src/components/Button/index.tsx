import * as React from "react"
import css from "../../css"
import {Field, IField} from "../Form/Field"
import {colord as Color} from "colord"
import Theme from "../Theme"
import Icon from "../Icon"
import ActionButton from "./action-button"

export interface Props extends IField {
    type?: "button" | "submit" | "void" | "href"
    link?: string
    to?: string
    icon?: any
    color?: "blue" | "gray" | "grey" | "green" | "red" | "nocolor" | "yellow" | "orange" | "transparent" | "decoration" | string
    size?: "small" | "smaller" | "tiny" | string
    disabled?: boolean
    onClick?: (evt: any) => any
    secondary?: boolean
    children?: any
    history?: any
    onClickFn?: Function
    onClickArgs?: any
    rounded?: boolean
    simple?: boolean
    actions?: any[]
    [propName: string]: any,
}


const ModifyChildren = (children, extraProps) => {
    if(!children || typeof children !== "object") {
        return children
    }
    if(Array.isArray(children)) {
        return children.filter(o => o).map(kid => {
                            return AddExtraProps(kid, extraProps)
                        })
    } else {
        return AddExtraProps(children, extraProps)
    }

}

function AddExtraProps(Component, extraProps) {
    if(typeof Component === "string") {
        return Component
    }
    if(Array.isArray(Component)) {
        return Component
    }
    const children = ModifyChildren(Component.props.children, extraProps)

    return React.cloneElement(Component, { ...extraProps, ...Component.props, children,  })

}

export class Button extends React.PureComponent<Props, {}> {
    protected readonly name = "muk-button";

    public static defaultProps: Props = {
        type: "button",
        icon: null,
        color: "blue",
        disabled: false,
        size: "",
        secondary: false,
        rounded: false,
        onClickArgs: [],
    }

    onClick = (e) => {
        if(this.props.onClickFn) {
            this.props.onClickFn(...this.props.onClickArgs)
        }
        if(this.props.onClick) {
            this.props.onClick(this.props.value ?? e)
        }
        if(this.props.history && this.props.to) {
            this.props.history.push(this.props.to)
        }
    }

    public renderButton() {
        const { className, to, link, label, children, type, onClickFn,onClickArgs, actions, ...props } = this.props
        const iconColor = props.color === "nocolor" ? Theme.blue : props.secondary || props.simple ? (Theme[props.color] || props.color) : "white"
        const icon = typeof props.icon === "string" ? <Icon color={iconColor} type={props.icon} />
                        : props.icon ? props.icon
                        : children ? children
                        : <Icon color={iconColor} type={null} />

        const content = AddExtraProps(
                        icon,
                        {
                            color: iconColor,
                            ...((label || children) ? { text: label ? <span>{label}</span> : AddExtraProps(children, {color: iconColor, type: null}) }: {}),
                        }
        )

        switch(type) {
            case "href":
                return <StyledHref {...props}
                            className={`muk-button m-button-href ${props.secondary ? "secondary" : ""} ${className ? className : ""}`}
                            href={to ? to : link}>
                                {content}
                        </StyledHref>
            case "button":
                return <StyledButton {...props}
                            onClick={this.onClick}
                            className={`muk-button m-button ${props.secondary ? "secondary" : ""} ${className ? className : ""}`}>
                                {content}
                        </StyledButton>

            case "void":
                return <StyledSpan {...props}
                            onClick={this.onClick}
                            className={`muk-button m-button- ${props.secondary ? "secondary" : ""} ${className ? className : ""}`}
                        >
                            {content}
                        </StyledSpan>
            case "submit":
                const p:any = {...props}
                delete p.style
                return <CssField className={`m-field   m-${props.color} ${!label ? `m-notext`:``} ${props.size ? `m-${props.size}` : ``} ${this.name}--${type} ${props.disabled ? `m-disabled`:``}`}
                            {...this.props}
                            onClick={this.onClick}
                            name={this.name}
                            style={{ marginBottom: 0, ...props.style }}
                            >
                                <StyledInput {...p}
                                    className={`muk-button ${props.secondary ? "secondary" : ""} m-button-submit ${className ? className : ""}`}
                                    type="submit"
                                    name={props.name}
                                    value={children}
                                />
                        </CssField>
            default:
                return false
        }
    }

    render() {
        const {actions, color, size} = this.props
        if(!actions) {
            return this.renderButton()
        }
        return <>
            {this.renderButton()}
            <ActionButton actions={actions} color={color}  size={size}/>
        </>
    }
}

const CssField = css(Field)({
    display: "inline-block",
    verticalAlign: "top",
})

const StyledButton = css("button")({
    boxSizing: "border-box",
    display: "inline-block",
    cursor: "pointer",
    textAlign: "center",
    textDecoration: "none",
    userSelect: "none",
    borderWidth: "1px",
    borderStyle: "solid",
    " .muk-icon__text": {
        " .muk-icon .m-image": {
            display: "none",
        },
    },
    " .muk-icon": {
        display: "inline",
    },
    ":hover": {
      textDecoration: "none",
    },
    ":last-child": {
        marginRight: 0,
    },
    " .muk-icon + span": {
        marginLeft: "4px",
    },
    " span + .muk-icon": {
        marginRight: "4px",
    },
    " > span": {
        verticalAlign: "middle",
    },
    " .muk-popup_hint": {
        verticalAlign: "middle !important",
    },
    " a": {
        color: "white",
    },
}, ({size,...props}) => {
    let disabled = {}
    if(props.disabled) {
        disabled = {
            opacity: 0.5,
            cursor: "default",
            pointerEvents: "none",
        }
    }
    let color = {}
    if(props.color === "nocolor") {
        color = {
            background: "transparent",
            padding: "0 2px",
            borderColor: "transparent",
            color: Theme.blue,
            "&:active,&:focus": {
              borderColor: "transparent",
              outline: "none",
              background: "rgba(200,200,200,0.2)",
            },
        }
    } else {
        let c = Theme[props.color === "gray" ? "grey" : props.color] || props.color || Theme.blue
        if(c === Theme.grey) {
            c = Color(c).darken(0.2).toHex()
        }
        if(props.secondary) {
            color = {
                backgroundColor: "transparent",
                borderColor: c,
                color: props.color === "decoration" ? Theme.blue : c,
                "&:hover": {
                    backgroundColor: Color(c).alpha(0.2).toRgbString(),
                    borderColor: Color(c).toHex(),
                },
                "&:active,&:focus": {
                  background: Color(c).alpha(0.8).toRgbString(),
                  borderColor: Color(c).alpha(0.8).toRgbString(),
                  color: "white",
                },
            }
        } else if(props.simple) {
            color = {
                border: "none",
                background: "transparent",
                color: c,
                "&:hover": {
                    color: Color(c).alpha(0.5).toRgbString(),
                },
                "&:active,&:focus": {
                    color: Color(c).alpha(0.2).toRgbString(),
                    border: "none",
                },
            }
        } else {
            color = {
                backgroundColor: c,
                borderColor: c,
                color: props.color === "decoration" ? Theme.blue : "white",
                boxShadow: "rgb(0 0 0 / 30%) 1px 1px 3px",
                ":hover": {
                    backgroundColor: Color(c).darken(0.1).toHex(),
                    borderColor: Color(c).darken(0.1).toHex(),
                },
                ":active,:focus": {
                },
            }
        }
    }

    return {
        ...disabled,
        ...color,
        textTransform: Theme.button_text_transform,
        fontWeight: Theme.button_text_weight,
        fontSize: size === "small" ? "14px" : size === "smaller" ? "13px" : size === "tiny" ? "11px" : "16px",
        borderRadius: props.rounded ? "999em" : Theme.radius,
        padding: size === "small" ? "6px 25px 7px 25px" : size === "smaller" ? "5px 12px" : size === "tiny" ? "2px 2px" : "11px 30px 12px 30px"
    }
})

const StyledHref = StyledButton.withComponent('a')
const StyledSpan = StyledButton.withComponent('span')
const StyledInput = StyledButton.withComponent('input')


export default Button
