Example

    <>
    <Section color="warning" header="Whatever header" controls={[<span className="block"><Button size={"smaller"} simple={true} icon="camera"></Button></span>,
    <span className="block"><Button size={"small"} simple={true}>Historie</Button></span>]}>
        Any content you want
    </Section>
    <Section header="Whatever header" color="steelblue" controls={[<span className="block"><Button size={"small"} simple={true} color="white">Akce</Button></span>,
    <span className="block"><Button size={"small"} simple={true} color="white">Historie</Button></span>]}>
        Any content you want
    </Section>
    <Section header="Whatever header" color="red" prefix={<span><Icon name="IconChevronLeft" /> Zpět</span>} >
        Any content you want
    </Section>
    <Section header={<>Whatever header<br/>Hello</>} suffix={<span>Vpřed <Icon name="IconChevronRight" /></span>}>
        Any content you want
    </Section>
    <Section header="Whatever header" prefix={<Button icon="chevron-left" color="gray">Zpět</Button>}  suffix={<Button color="green" icon="chevron-right">Vpřed</Button>}  >
        Any content you want
    </Section>

    <Section header="Whatever header"  suffix={<span><Icon name="IconChevronRight" /><br/>Vpřed</span>}  >
        Any content you want
    </Section>

    <Section>
        Section without header
    </Section>

        <Section color="brown" header={<Nav><NavLink active={true}>Hello</NavLink><NavLink>World</NavLink></Nav>}>
            Section with nav
        </Section>
    </>

Inverted

    <>


        <Section inverted={true} header={<Nav><NavLink active={true}>Hello</NavLink><NavLink>World</NavLink></Nav>}>
            Section with nav
        </Section>


        <Section inverted={true} header="Whatever header">
            Any content you want
        </Section>
        <Section inverted={true} header="Whatever header" prefix={<span><Icon name="IconChevronLeft" /> Zpět</span>} >
            Any content you want
        </Section>
        <Section inverted={true} header="Whatever header"  suffix={<span>Vpřed <Icon name="IconChevronRight" /></span>}  >
            Any content you want
        </Section>
        <Section inverted={true} header="Whatever header" prefix={<span><Icon name="IconChevronLeft" /> Zpět</span>}  suffix={<span>Vpřed <Icon name="IconChevronRight" /></span>}  >
            Any content you want
        </Section>

        <Section inverted={true} >

            <Section header="Inside head">
                Any content you want
            </Section>

        </Section>

    </>
