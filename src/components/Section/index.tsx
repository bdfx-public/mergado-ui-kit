import * as React from "react"
import Grid from "../Grid"
import GridCell from "../GridCell"
import css from '../../css'
import Theme from "../Theme"
import {colord as Color} from "colord"

interface Props {
    header?: JSX.Element | string
    prefix?: JSX.Element | string
    suffix?: JSX.Element | string
    children?: any
    inverted?: boolean
    className?: string
    color?: string
    style?: any
    controls?: any
}



class Section extends React.PureComponent<Props,{}> {
    name = "muk-section"

    isDark = (color) => {
        return Color(color).brightness() < 0.7;

    }

    render () {
        const { children, header, prefix, suffix, className, color, controls, ...p } = this.props
        const background = color ? (Theme[color] || color) : "#f9f9f9"
        const isDark = background ? this.isDark(background) : false
        return (
            <CssSection className={`${name} ${className || ""}`} {...p}>
                {controls && <Controls className="m-controls" isDark={isDark}>{controls}</Controls>}
                {header ?
                        <CssHeader background={background} isDark={isDark} inverted={p.inverted} className={"m-header"} cols={`${prefix ? "auto" : ""} 1fr ${suffix ? "auto" : ""}`}>
                            {prefix && <CssPrefix isDark={isDark} valign="center" className="m-prefix">{prefix}</CssPrefix>}
                            <GridCell valign="center" className="m-header-content">
                                {header}
                            </GridCell>
                            {suffix && <CssSuffix  isDark={isDark} valign="center" className="m-suffix">{suffix}</CssSuffix>}
                        </CssHeader>
                : false}
                <div className="m-content">
                    {children}
                </div>
            </CssSection>
        )
    }
}



const Controls = css("div")({
    position: "absolute",
    right: 0,
    " .block": {
        verticalAlign: "top",
        border: "1px solid white",
        borderTop: "none",
        borderRight: "none",
        height: "30px",
        lineHeight: "28px",
        display: "inline-block",
    },
    " .block:first-of-type": {
        borderBottomLeftRadius: "5px",
    },
    " .block:last-of-type": {
        borderRight: "1px solid white",
        borderBottomRightRadius: "5px",
        marginRight: "15px",
    }
}, ({isDark}) => ({
    top: isDark ? "-1px" : 0,
    " .block": {
        borderColor: isDark ? undefined : (Theme.decoration + "!important"),
        background: !isDark ? "white" : undefined,
    }
}))

const CssSection = css("section")({
    position: "relative",
    marginBottom: "20px",
    color: Theme.text,
    borderRadius: Theme.radius,
    border: Theme.section_border,
    " .m-content": {
        padding: Theme.section_padding,
    },
}, ({inverted}) => ({
    background: inverted ? "#f9f9f9" : Theme.background,
}))

const CssHeader = css(Grid)({
    fontWeight: "normal",
    fontSize: "15px",
    borderRadius: `${Theme.radius} ${Theme.radius} 0 0`,
    " .muk-nav": {
        paddingTop: 0,
        " .muk-navlink": {
            borderRadius: " 0 0 3px 3px",
            marginRight: "5px",
        },
        " .muk-navlink a": {
            border: "none",
            padding: "12px 20px",
            borderRadius: " 0 0 3px 3px",
        },
        " .active": {
            border: "none",
            boxShadow: "1px 1px 3px rgba(0,0,0,.3)",
            background: "white",
            color: "#333",
        },
    },
}, ({inverted, isDark, background}) => ({
    margin: isDark ? "-1px" : undefined,
    color: isDark ? "white" : undefined,
    background:  background,
    borderBottom: isDark || inverted ? undefined : Theme.section_border,
    " .m-header-content": {
        padding: inverted ? undefined : `16px 20px`,
    },
    " .muk-nav": {
        marginTop: inverted ? "-3px" : "-20px",
    },
    " .muk-navlink:not(.active) a": {
        color: isDark ? "white" : undefined,
    },
}))



const CssPrefix = css(GridCell)({
    padding: "16px 20px",
    marginRight: "15px",
    textAlign: "center",
    height: "100%",
    borderRight: "1px solid rgba(0,0,0,0.1)",
}, ({isDark}) => ({
    background: isDark ? "rgba(0,0,0,0.1)" :  "rgba(0,0,0,0.01)" ,

}))

const CssSuffix = css(GridCell)({
    padding: "16px 20px ",
    marginLeft: "15px",
    textAlign: "center",
    height: "100%",
    borderLeft: "1px solid rgba(0,0,0,0.1)",
}, ({isDark}) => ({
    background: isDark ? "rgba(0,0,0,0.1)" : "rgba(0,0,0,0.01)" ,
}))


export default Section
