import * as React from "react"
import css from "../../css"

interface Props {
    children?: any
    col?: string | number
    row?: string | number
    name?: string

    align?: "start" | "end" | "center" | "stretch" | "left" | "right"
    valign?: "start" | "end" | "center" | "stretch" | "top" | "bottom"

    autoFlow?: "row" | "column"
    style?: any
    className?: string
    colSpan?: string
    rowSpan?: string
}

const GridCell = (props: Props) => {
    const { children, className, ...p } = props

    return (
        <GridCellStyle className={`muk-gridcell ${className || ""}`} {...p}>
            {children}
        </GridCellStyle>
    )
}

const GridCellStyle = css("div")({

},(props: any) => {
    return {
        gridColumn: props.col ? props.col : "auto auto",
        gridRow: props.row ? props.row : "auto auto",
        gridArea: props.name ? props.name : null,
        justifySelf: props.align ? (props.align === "left" ? "start" : props.align === "right" ? "end" : props.align ) : "stretch",
        alignSelf: props.valign ? (props.valign === "top" ? "start" : props.valign === "bottom" ? "end" : props.valign ) : "stretch",
    }
})

export default GridCell
