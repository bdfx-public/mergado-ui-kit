import * as React from "react"
import css from "../../../css"

interface Props {
    rowId?: string | number
    isOdd?: boolean
    isSelected?: boolean
    className?: string
    whiteSpace?: string
    align?: string
    valign?: string
    width?: string | number
    height?: string | number
    row?: string | number
    col?: string | number
    title?: string
    sortBy?: string
    k?: string
    style?: any
}

class Cell extends React.Component<Props, {}> {
    shouldComponentUpdate(prev) {
        return this.props.isOdd !== prev.isOdd || this.props.rowId !== prev.rowId || this.props.isSelected !== prev.isSelected
    }

    public render() {
        const { isOdd, rowId, children, className, ...props } = this.props
        return <BodyCell className={`muk-cell ${rowId ? ("row-"+rowId) : ""} ${isOdd ? "row-odd" : "row-even"} ${className || ""}`} {...props} >
                    {children}
                </BodyCell>
    }
}

const BodyCell = css("div")({
}, ({align, valign, whiteSpace, row, col }) => ({
    whiteSpace: whiteSpace || "nowrap",
    justifyContent: align === "left" ? "flex-start" : align === "right" ? "flex-end" : (align || "center"),
    alignItems: valign === "top" ? "flex-start" : valign === "bottom" ? "flex-end" : (valign || "center"),
    gridRow: row,
    gridColumn: col,
}))

export default Cell
