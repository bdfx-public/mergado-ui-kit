import * as React from "react"
import Checkbox from "../../Form/Checkbox"
import {isFragment} from 'react-is';

import Cell from "../Cell"

interface Props {
    rowId?: number | string
    selectRow?: (rowId: number|string) => void
    isSelected?: boolean
    isOdd?: boolean
    className?: string
}
interface State {
    isSelected: boolean
}

class Row extends React.Component<Props, State> {

    state = {
        isSelected: false
    }

    shouldComponentUpdate(prev) {
        return this.props.isOdd !== prev.isOdd || this.props.rowId !== prev.rowId || this.props.isSelected !== prev.isSelected
    }

    static getDerivedStateFromProps(props, state) {
        if(props.isSelected !== state.isSelected) {
            return ({ isSelected: props.isSelected })
        }
        return null
    }

    renderCell = (o,i) => {

        if(isFragment(o)) {
            return o.props.children.map(this.renderCell)
        }

        const { isOdd, rowId, className } = this.props

        const {children, k, ...props} = typeof o === "object" ? o.props : {children: o, k: o,}

        return <Cell {...props} className={`${className || ""} ${props.className || ""}`} key={rowId + "-" + (k ? k : i)} isOdd={isOdd} rowId={rowId}>
                    {children}
                </Cell>
    }

    selectRow = () => this.props.selectRow(this.props.rowId)

    public render() {
        const { children, selectRow, rowId, isOdd, className, } = this.props
        const {isSelected} = this.state
        const cells = children && Array.isArray(children) ? children : [children]
        return (<>
            {selectRow && (
                <Cell isOdd={isOdd} className={className} rowId={rowId} isSelected={isSelected}>
                    <Checkbox checked={isSelected} onChange={this.selectRow} />
                </Cell>
            )}
            {cells.map(this.renderCell)}
        </>)
    }
}

export default Row
