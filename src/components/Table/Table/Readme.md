Example

    <Table ids={[1,2,3,4,5,6]} style={{position: "relative"}} preheader={<Cell col="1 / -1" style={{float: "right", zIndex: 1000, position: "absolute", backgroundColor: "#333!important", color: "white", padding: 0, right: 0,}}>Preheader</Cell>} stickyHeader={true} actions={() => false} sortColumn={"first"} sortDirection={"desc"} header={[
            <span align="left" width="1fr" k="1" sortBy="first">FirstFirstFirst</span>,
            <span k="2" width="1fr" sortBy="second">Second</span>,
            <span k="3" sortBy="third">Third</span>,
        ]}
    >{(selected,toggle) => [1,2,3,4,5,6].map((i,j) => <Row rowId={i} isSelected={selected.indexOf(i) > -1} selectRow={toggle} isOdd={j%2 === 0}>
            <Cell align="left">{i}-1</Cell>
            <Cell>{i}-2</Cell>
            <Cell>{i}-3</Cell>
        </Row>)}
    </Table>

Example

    <Table stickyHeader={true} header={[
            "First",
            "Second",
            "Third",
            "Fourth",
            "Fifth",
            "Sixth",
            "Seventh",
            "Eighth",
            "Nineth",
            "Tenth",
        ]}>
        <span align="left">1-1</span>
        <span>1-2</span>
        <span>1-3</span>
        <span>1-4</span>
        <span>1-5</span>
        <span>1-6</span>
        <span>1-7</span>
        <span>1-8</span>
        <span>1-9</span>
        <span>1-10</span>
        <span align="center" valign="center">2-1</span>
        <span>2-<br/>-2<br/>-?</span>
        <span>2-3</span>
        <span>1-4</span>
        <span>1-5</span>
        <span>1-6</span>
        <span>1-7</span>
        <span>1-8</span>
        <span>1-9</span>
        <span>1-10</span>
        <span align="center" valign="center">3-1</span>
        <span>3-2</span>
        <span>3-3</span>
        <span>1-4</span>
        <span>1-5</span>
        <span>1-6</span>
        <span>1-7</span>
        <span>1-8</span>
        <span>1-9</span>
        <span>1-10</span>
        <span align="center" valign="center">4-1</span>
        <span>4-2</span>
        <span>4-3</span>
        <span>1-4</span>
        <span>1-5</span>
        <span>1-6</span>
        <span>1-7</span>
        <span>1-8</span>
        <span>1-9</span>
        <span>1-10</span>
        <span align="center" valign="center">5-1</span>
        <span>5-2</span>
        <span>5-3</span>
        <span>1-4</span>
        <span>1-5</span>
        <span>1-6</span>
        <span>1-7</span>
        <span>1-8</span>
        <span>1-9</span>
        <span>1-10</span>
        <span align="center" valign="center">6-1</span>
        <span>6-2</span>
        <span>6-3</span>
        <span>1-4</span>
        <span>1-5</span>
        <span>1-6</span>
        <span>1-7</span>
        <span>1-8</span>
        <span>1-9</span>
        <span>1-10</span>
    </Table>