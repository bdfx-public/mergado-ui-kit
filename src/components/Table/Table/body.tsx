import * as React from "react"
import {isElement,isFragment} from 'react-is';

import BodyCell from "../Cell"

interface Props {
    cells: any[]
    cols: number
}
interface State {
}

class Body extends React.PureComponent<Props, State> {

    renderBodyCell = (o,i) => {

        if(isFragment(o)) {
            return o.props.children.map(this.renderBodyCell)
        }
        const { cols, } = this.props
        const {children, k, ...props} = typeof o === "object" ? o.props : {children: o, k: o}
        const rowNumber = Math.floor(i / cols)
        const isOdd = (rowNumber-1)%2
        return <BodyCell {...props} key={rowNumber+"-"+(typeof children === "string" ? children : k ? k : i)} isOdd={isOdd}>
                    {children}
                </BodyCell>
    }

    public render() {
        const { cells } = this.props
        return cells.map(this.renderBodyCell)
    }
}

export default Body

