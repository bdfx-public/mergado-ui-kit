import * as React from "react"
import css from "../../../css"
import Grid from "../../Grid"
import GridCell from "../../GridCell"
import pull from "lodash/pull"
import Header from "./header"
import Body from "./body"

interface Props {
    children: Function | JSX.Element
    preheader?: JSX.Element
    postheader?: JSX.Element
    postbody?: JSX.Element
    header: any[]
    ids?: number[]
    actions?: (selected: number[]) => JSX.Element
    showActions?: boolean,
    showActionsBelow?: boolean | Function

    sortColumn?: string
    sortDirection?: "asc" | "desc"
    setSort?: (column: string, direction: "asc"|"desc") => void
    style?: any
    className?: string

}
interface State {
    selected: number[]
}

class Table extends React.PureComponent<Props, State> {
    readonly name = "muk-table"

    state = {
        selected: [],
    }

    toggleRow = (id) => this.setState(s => {
        const selected = [...s.selected]
        const index = s.selected.indexOf(id)
        if(index > -1) {
            selected.splice(index,1)
        } else {
            selected.push(id)
        }
        return ({
            selected,
        })
    })

    toggleAllRows = () => this.setState(s => ({
        selected: s.selected.length !== this.props.ids.length ? this.props.ids : []
    }))

    setSort = column => this.props.setSort ? this.props.setSort(column, this.props.sortColumn === column && this.props.sortDirection === "asc" ? "desc" : "asc") : console.error('Not set sorting callback')

    countCols = () => [...(this.props.actions !== undefined && this.props.ids !== undefined ? [{props: {width: "50px"}}]: []),...this.props.header].map(h => {
        if(typeof h === "object" && h.props && h.props.width) {
            return h.props.width
        }
        return "1fr"
    }).join(" ")

    public render() {
        const { header, preheader, postbody,postheader, actions, ids, sortColumn, sortDirection, style, showActions,showActionsBelow, className } = this.props
        const { selected, } = this.state
        const children:any = this.props.children
        const isActionable = actions !== undefined
        return (
            <>
            {isActionable && (selected.length > 0 || showActions) && actions(selected)}
            <Styled className={`muk-table ${className || ""}`} cols={this.countCols()} style={style}>
                {preheader}
                <Header header={header}
                        setSort={this.setSort}
                        sortedBy={sortColumn}
                        sortedDirection={sortDirection}
                        selectAll={isActionable && ids ? this.toggleAllRows : undefined}
                        selectedAll={isActionable && ids ? selected.length === ids.length : false} />
                {postheader}
                {typeof children === "function" ? children(selected, this.toggleRow, header.length)
                    : <Body cells={Array.isArray(children) ? children : [children]}
                            cols={header.length} />
                }
                {postbody}
            </Styled>
            {isActionable && (selected.length > 0 || showActions) && showActionsBelow && (typeof showActionsBelow === 'function' ? showActionsBelow(selected) : actions(selected))}
            </>
        )
    }
}

const Styled = css(Grid)({
    marginTop: "5px",
    marginBottom: "5px",
    borderBottom: "3px solid #eee",
    // borderRight: "1px solid #eee",

    ".muk-cell": {
        display: "flex",
        // borderLeft: "1px solid #eee",
        padding: "8px",
        borderBottom: "1px solid #eee",
        background: "#fff",
        "&.row-even": {
            background: "#fafafa",
        },

    },

    ".m-header-cell": {
        padding: "2px 4px 0 6px",
        fontWeight: "normal",
        textTransform: "uppercase",
        background: "#f9f9f9",
        fontSize: "11px",
        position: "sticky",
        top: 0,
        overflow: "hidden",
        marginRight: "-1px",
        borderLeftWidth: "0",
        border: "1px solid #ddd",
        overflowX: "hidden",
        textOverflow: "ellipsis",
        whiteSpace: "nowrap",
        minWidth: "25px",
        zIndex: 1,
        "&.active": {
            background: "#f0f0f0",
        },
        "&.sortable": {
            paddingRight: "18px",
            minWidth: "50px",
            cursor: "pointer",
            ":hover": {
                background: "#f1f1f1",
            },
        },

        ".sort-icon": {
            position: "absolute",
            right: "3px",
            top: "0px",
        },

        ".select-all": {
            marginBottom: "-3px",
        },

    },
})

export default Table
