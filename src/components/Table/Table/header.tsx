import * as React from "react"
import Checkbox from "../../Form/Checkbox"
import {isElement} from 'react-is';

import HeaderCell from "./header-cell"

interface Props {
    header: any[]

    sortedBy?: string
    sortedDirection?: "asc" | "desc"
    setSort?: (name) => void
    selectedAll?: boolean
    selectAll?: () => void
}
interface State {
}

class Header extends React.PureComponent<Props, State> {


    renderHeaderCell = (o) => {

        if(!isElement(o) && o.props) {
            return o.props.children.map(this.renderHeaderCell)
        }

        const {sortedBy, setSort, sortedDirection} = this.props
        const {children, sortBy, id, ...props} = typeof o === "object" ? o.props : {children: o, sortBy: null, id: o}
        const sorted = sortBy && sortedBy === sortBy ? sortedDirection : false

        return  <HeaderCell key={id} {...props} sortBy={sortBy} sorted={sorted} setSort={setSort}>
                    {children}
                </HeaderCell>
    }

    public render() {
        const { header,selectedAll, selectAll, } = this.props
        return (<>
            {selectAll && (
                <HeaderCell>
                    <Checkbox className="select-all" checked={selectedAll} onChange={selectAll} />
                </HeaderCell>
            )}
            {header.map(this.renderHeaderCell)}
        </>)
    }
}

export default Header
