import * as React from "react"
import css from "../../../css"
import GridCell from "../../GridCell"
import Theme from "../../Theme"
import Icon from "../../Icon"

interface Props {
    sortBy?: string
    sorted?:  false | "asc" | "desc"
    setSort?: (column:string) => void
    onClick?: (column, direction) => void
    className?: string
}
interface State {
    active: boolean
}

class HeaderCell extends React.PureComponent<Props, State> {

    state = {
            active: false,
    }
    static getDerivedStateFromProps(props, state) {
        if(!!props.sorted !== state.active) {
            return ({ active: !!props.sorted })
        }
        return null
    }

    setSortColumn = () => {
        if(this.props.onClick) {
            this.props.onClick(this.props.sortBy, this.props.sorted)
        }
        if(this.props.sortBy) {
            this.setState({active: true});
            if(this.props.setSort) {
                this.props.setSort(this.props.sortBy)
            }
        }
    }

    public render() {
        const { sortBy, sorted, children, setSort, className, ...props } = this.props
        const sortable = sortBy && setSort !== undefined
        const {active} = this.state
        return <Cell {...props} className={`m-header-cell ${sortable ? "sortable" : ""} ${active ? "active" : ""} ${className || ""}`}
                            onClick={this.setSortColumn} >
                    {children}
                    {sortable &&
                    <>
                        {!sorted && <Icon type="sort" className="sort-icon" color={"#ddd"} />}
                        {sorted && ( sorted === "desc" ? <Icon className="sort-icon" type="sort-desc" color="#444" size={14} /> : <Icon  className="sort-icon" type="sort-asc" size={14} color="#444"  /> )}
                    </>
                    }
                </Cell>
    }
}

const Cell = css("div")({

}, props => ({
    textAlign: props.align ? props.align : "center",
    verticalAlign: props.valign ? props.valign : "middle",
    width: props.width,
    height: props.height,
}))

export default HeaderCell
