import * as React from "react"
import css from "../../css"
import Theme from "../Theme"
import debounce from "lodash/debounce"

export interface Props {
    underline?: boolean
    history?: any
    to?: string
    style?: any
    color?: string
    className?: string
    onClick?: (event) => boolean|void
    onDoubleClick?: (event) => boolean|void
    onClickFn?: Function
    onClickArgs?: any
    [propName: string]: any
}

export interface State {
}

class Link extends React.PureComponent<Props, State> {

    private readonly name = "muk-link";

    performAction = debounce((e) => {
        if(this.props.onClickFn) {
            this.props.onClickFn(...this.props.onClickArgs)
        }
        if(this.props.history) {
            this.props.history.push(this.props.to)
        }
        if(this.props.onClick) {
            this.props.onClick(e)
        }
        return false
    },200)

    onClick = e => {
        e.persist()
        e.preventDefault()
        e.stopPropagation()
        this.performAction(e)
    }

    public render() {
        const {className, children, onClickArgs, onClickFn, ...p} = this.props
        return (
            <CssLink
                    {...p}
                    href={p.to || p.href ? ((p.href || "") + (p.to || "")) : undefined}
                    className={`${this.name} ${this.props.className || ""}`}
                    onClick={this.onClick}>
                {children}
            </CssLink>
        )
    }
}

const CssLink = css("a")({
    cursor: "pointer",
    color: Theme.blue,
    "&:visited,&:focus": {
        textDecoration: "none",
    },
}, props => ({
    textDecoration: props.underline ? "underline" : "none",
    color: !props.color ? Theme.blue : (Theme[props.color] !== undefined) ? Theme[props.color] : props.color,
    "&:hover,&:active": {
        color: !props.color ? Theme.blue : (Theme[props.color] !== undefined) ? Theme[props.color] : props.color,
        textDecoration: props.underline === false ? "none" : "underline",
    },
}))

export default Link
