Example link

    <Link to="https://google.com" underline={false} onClickFn={console.log} onClickArgs={["Clicked!"]}>I'm link</Link><br/>
    <Link to="https://google.com" underline={true} onClick={() => alert('Clicked')}>Underlined link</Link><br/>
    <Link to="https://google.com" color="success" onClick={() => alert('Clicked')}>Colorized link</Link>
