import * as React from "react"
import css from "../../css"

import uniqueId from "../../helpers/unique_id"
import Button from "../Button"
import {Type} from "../../helpers/types"
import Grid from "../Grid"
import Theme from "../Theme"
import Header from "../Header"

export interface Props {
    id?: string,
    title?: string | JSX.Element,
    text?: string | JSX.Element,
    type?: Type,
    icon?: JSX.Element
    onClose?: (id: string) => boolean
    timeout?: number
    closeable?: boolean
    style?: any
    className?: string
    onClick?: (e:any) => void
    hide?: boolean
    inline?: boolean

}
export interface State {
    visible: boolean,
    removed: boolean
}

class Toast extends React.PureComponent<Props, State> {
    private readonly name = "muk-toast";
    private countdown;

    public static defaultProps: Props = {
        id: uniqueId(),
        text: "",
        type: "info",
        icon: null,
        onClose: () => { return true },
        timeout: 0,
        closeable: true,
        style: {},
    }

    state = {
            visible: true,
            removed: false,
    }

    public componentDidMount() {
        if(this.props.timeout > 0) {
            this.countdown = setTimeout(this.timer,this.props.timeout)
        }
    }

    timer = () => {
        this.hideToast()
        setTimeout(o => this.removeToast(), 300)
    }

    hideToast = () => {
        this.setState({
            visible: false,
        })
    }

    public componentDidUpdate(pp) {
        if(!pp.hide && this.props.hide) {
            this.hideToast()
        }
    }

    public componentWillUnmount() {
        this.hideToast()
        if(this.countdown) {
            clearTimeout(this.countdown)
        }
    }

    protected removeToast() {
        this.setState({
            removed: true,
        })
    }

    onClose = (evt) => {
        evt.preventDefault()
        if(this.props.onClose(this.props.id) === true) {
            this.hideToast()
            setTimeout(o => this.removeToast(), 300)
        }
        evt.stopPropagation()
    }

    onClick = (e) => {
        if(this.props.onClick) {
            this.onClose(e)
            this.props.onClick(e)
        }
    }

    public render() {
        const { inline, title, type, onClick, style, text, children,className,icon,closeable,...others} = this.props
        const {removed, visible} = this.state
        return (
            <Wrapper type={type}
                    inline={inline}
                    onClick={onClick ? this.onClick : undefined}
                    cols={"auto 1fr 40px"}
                    valign="center"
                    removed={removed}
                    s={style}
                    hidden={!visible}
                    className={`${this.name}__wrapper ${visible ? "" : "ended"} ${className || ""} ${removed ? "removed" : ""}`}
                    >
                    <Icon className={`${this.name}__icon`}>{icon}</Icon>
                    <Content className={`${this.name}__content`} hasTitle={!!title}>
                        {title && <Header type="4">{title}</Header>}
                        <span className={title ? "text" : ""}>
                            {text && typeof text == "string" ?
                                text
                            :
                                children }
                        </span>
                    </Content>
                    {closeable &&
                            <CloseButton type={type}>
                                <Button className={`${this.name}__button`}
                                    color="nocolor"
                                    size="tiny"
                                    type="void"
                                    onClick={this.onClose}
                                >×</Button>
                            </CloseButton>
                    }
            </Wrapper>
        )
    }
}

/* <style> */
const Wrapper = css(Grid)({
    width: "100%",
    transform: "translate3d(0,0,0)",
    willChange: "max-height",
    border: "0px solid transparent",
    " .muk-header": {
        marginTop: "0"
    },
    " .text": {
        fontSize: "90%",
    }
},(props: any) => {
    const type = props.type ? props.type : "info"
    const fullcolor =  type === "material" || type === "ok" || type === "fail"
    return {
        cursor: props.onClick !== undefined ? "pointer" : undefined,
        transition: props.removed ? "max-height 0.2s !important" : undefined,
        margin: props.removed ? "0px" : "10px 0",
        boxShadow: props.inline ? undefined
                 : props.type === "success" ? "rgba(0, 0, 0, 0.3) 2px 3px 5px 0px"
                 : props.type === "error" ? "rgba(0, 0, 0, 0.3) 2px 3px 5px 0px"
                                            : "rgba(0, 0, 0, 0.3) 2px 3px 5px 0px",
        border: !fullcolor && props.inline ? "1px solid #ddd" : undefined,
        borderRadius: "3px",
        opacity: props.hidden ? 0 : 1,
        borderLeft: fullcolor ? undefined : "10px solid",
        background: type === "material" ? "#222" : type === "ok" ? Theme.green : type === "fail" ? Theme.red : "white",
        borderColor: type === "transparent" ? "transparent" : type === "info" ? Theme.blue  : Theme[type],
        color: fullcolor ?"white" : undefined,
        " .text": {
            color: fullcolor ? undefined : "#888",
        },
        ...props.s,
        maxHeight: props.removed ? 0 : undefined,
    }
})


const Icon = css("div")({
    padding: "0 15px",
    alignSelf: "center",
})

const Content = css("div")(props => ({
    padding: props.hasTitle ? "12px 0px" : "18px 0",
    lineHeight: "1.2rem",

}))

const CloseButton = css("div")({
    padding: "5px 6px 0 10px",
    textAlign: "right",
    alignSelf: "start",
},(props: any) => {
    const type = props.type ? props.type : "info"
    const fullcolor =  type === "material" || type === "ok" || type === "fail"
    return {
        "& .muk-button": {
            fontSize: "18px",
            fontWeight: "normal",
            color:  fullcolor ? "white !important" : "#888 !important"
        },
    }
})

export default Toast
