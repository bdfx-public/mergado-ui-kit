import * as React from "react"
import { Props, show } from "../common"

export const modal = (props: Props) => {
    return show(props)
}

export default modal
