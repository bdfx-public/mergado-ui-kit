import * as React from "react"
import Theme from "../../Theme"
import { Props, show } from "../common"
import {TextInput} from "../../Form/TextInput"
import Icon from "../../Icon"

export const prompt = (props: Props) => {
    const message = typeof props === "object" ? props.message : props
    const onChange = (value) =>  prompt({...props,value})
    return show({
        ...props,
        icon: <Icon type="question-circle-o" size={50} color={Theme.blue} />,
        message: () => <div>
            {message}
            <TextInput value={props.value} onChange={onChange} />
        </div>,
        buttons: props.buttons instanceof Function ? props.buttons(props.value) : props.buttons,
    })
}

export default prompt
