import * as React from "react"
import Theme from "../../Theme"
import { Props, show } from "../common"
import Icon from "../../Icon"

export const confirm = (props: Props) => {
    const message = typeof props === "object" ? props.message : props
    return show({
        ...props,
        icon: <Icon type="question-circle-o" size={50} color={Theme.blue} />,
        message,
    })
}

export default confirm
