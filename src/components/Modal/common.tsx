import * as React from "react"
import { confirmAlert } from 'react-confirm-alert'; // Import
import Grid from "../Grid"
import css from "../../css"
import Button from "../Button"

export interface Props {
    title?: string | JSX.Element | false
    message?: string | JSX.Element | false | Function
    icon?: JSX.Element
    buttons?: any[] | Function | false
    onClose?: (value?) => any
    value?: any
    onSubmit?: (value?) => any
    willUnmount?: any
    className?: string
    style?: any
    hideCloseButton?: boolean,
}

export const show = ({title = false, message = false, buttons = false, icon, onClose, className, style, hideCloseButton, ...props }: Props) => confirmAlert({
      customUI: ({ onClose }) => <Div className={`react-confirm-alert-body ${className}`} style={style}>
              {hideCloseButton ? false : <CloseButton color="nocolor" size="tiny" onClick={onClose}>×</CloseButton> }
              {title && <Title>{title}</Title>}
              <Grid gap={"20px"} valign="center" cols={"auto 1fr"}>
              {icon}
              <div>
                  {message instanceof Function ? message(onClose) : message}
              </div>
              </Grid>
              {buttons &&
              <div className='react-confirm-alert-button-wrapper'>
                <div className='react-confirm-alert-button-group' onClick={onClose}>
                    {buttons}
                </div>
              </div>
              }
            </Div>,
        willUnmount: onClose ? onClose : undefined,
        ...props,
    })



const Title = css("h2")({
    margin: "-20px -30px",
    fontSize: "16px",
    fontWeight: "normal",
    padding: "10px 20px",
    marginBottom: "10px",
    fontStyle: "nromal",
    borderBottom: "1px solid #eee",
})

const Div = css("div")({
    "::-webkit-scrollbar": {
        width: "10px",
    },

    /* Track */
    "::-webkit-scrollbar-track": {
    border: "1px solid #ddd",
    borderRadius: "5px",
    background: "#eee",
    },

    /* Handle */
    "::-webkit-scrollbar-thumb": {
        background: "#888",
        borderRadius: "5px",
    },

    /* Handle on hover */
    "::-webkit-scrollbar-thumb:hover": {
        background: "#ccc",
    },
})

const CloseButton = css(Button)({
    float: "right",
    fontSize: "20px",
    fontWeight: "normal",
    color: "#888 !important",
    marginTop: "-25px",
    marginRight: "-15px",
})
