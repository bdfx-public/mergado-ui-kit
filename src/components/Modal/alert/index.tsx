import * as React from "react"
import Theme from "../../Theme"
import Button from "../../Button"
import { Props, show } from "../common"
import Icon from "../../Icon"


export const alert = (messageOrProps: Props) => {
    const message = typeof messageOrProps === "object" ? messageOrProps.message : messageOrProps
    return show({
        buttons: [<Button key="ok">OK</Button>],
        icon: <Icon type="exclamation-circle" size={50} color={Theme.warning} />,
        ...messageOrProps,
        message,
    })
}

export default alert
