Main icons

    <Section>
        <Icon type="important" text="Import Ant" size={12} />
        <Icon type="biddingfox" text="Bidding Fox" size={35} />
        <Icon type="dataowl" text="Data Owl" size={20} />
        <Icon type="feedimageeditor" text="Feed Image Editor" size={25} />
        <Icon type="reglendo" text="Reglendo" size={20} />
        <Icon type="mergado" text="Mergado" size={10} />
        <br/><br/>
        Text first:<br/>
        <Icon type="500px" text="500px" size={20} color="blue" textFirst={false} />
        <Icon type="500px" text="500px" size={20} color="blue" textFirst={true} />
        <br/><br/>
        Color: <br/>
        <Icon type="trophy" text="Trophy" size={20} color="gold" />
        <br/><br/>
        Icons with modified viewbox and width:<br/>
        <Icon type="zbozi-cz" size={30} width="120px" viewBox="0 0 220 40" />
        <br/><br/>
        Only text:<br/>
        <Icon type={null} size={30} text={"Empty"} />

    </Section>

Icon finder

    <Section>
        <IconFinder />
    </Section>
