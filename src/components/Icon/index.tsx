import * as React from "react"
import Span from "../Span"
import css from "../../css"
import Theme from "../Theme"
export interface Props {
    type?: string
    name?: string
    color?: string
    size?: number | string
    text?: string | JSX.Element
    style?: any
    title?: string | number

    className?: string
    viewBox?: string
    textFirst?: boolean

    width?: number
    height?: number
}

class Icon extends React.PureComponent<Props, {}> {

    private readonly name = "muk-icon";

    public static defaultProps: Props = {
        size: 15,
    }

    public render() {
        const { name, type, color, text, size, className, textFirst, width, height, children, ...p} = this.props
        const iconType = type !== undefined ? type : name.replace('Icon','').replace(/\B[A-Z]/g, s => "-"+s).replace(/\B[0-9]/g, s => "-"+s).toLowerCase()
        const textEl = text || children ? <Label className={'muk-icon__text'} color={color}>
                                    {text}{children}
                                </Label> : false
        return <Wrapper key={iconType}
                        noIcon={!iconType}
                        noText={textEl === false}
                        className={`${this.name} ${textEl === false ? "no-text" :""} ${!iconType ? "no-icon" :""} ${this.name}--${iconType} ${className || ""}`}
                        {...p}
                >
                            {textFirst && textEl}
                            {color || !iconType ?
                                <I className="m-image" width={(width || size)+"px"} height={(height || size)+"px"} color={color} type={iconType} />
                            :
                                <img className="m-image" src={`https://cdn.bdfx.cz/muk/icons/${iconType}.svg`} width={(width || size)+"px"} height={(height || size)+"px"} />
                            }
                            {!textFirst && textEl}
                        </Wrapper>
    }
}

const Label=css("span")(p => ({
    color: p.color ? (Theme[p.color] || p.color) : undefined,
}))

const Wrapper = css(Span)({
    display: "inline-block",
    " .m-image": {
        verticalAlign: "middle",
        display: "inline-block",
    },
}, ({noText, noIcon}) => ({
    " .m-image": {
        marginTop: noText ? undefined : "-2px",
        width: noIcon ? "0px" : undefined,
    },
    " .muk-icon__text + .m-image": {
        marginLeft: noIcon ? undefined : "4px",
    },
    " .m-image + .muk-icon__text": {
        marginLeft: noIcon ? undefined : "4px",
    },

}))



const I = css(Span)({
    position: "relative",
    display: "inline-block",

    ":before": {
        /* Positioning related code */
        content: '""',
        display: "block",
        width: "100%",
        height: "100%",
        pointerEvents: "none",

        /* this allows optical centering if required */
        maskPosition: "center",
        maskRepeat: "no-repeat",
        maskSize: "contain",

    },
}, ({type,color}) => ( type ? ({
    ":before": {
        maskImage: 'url("https://cdn.bdfx.cz/muk/icons/'+type+'.svg")',
        background: Theme[color] || color,
    },
}) : {
    width: "0px",
} ))

export default Icon
