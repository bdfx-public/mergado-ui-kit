import * as React from "react"
import {list} from "./list"
import css from "../../../css"

import Icon from ".."
import TextInput from "../../Form/TextInput"
import Button from "../../Button"
import Grid from "../../Grid"

export interface Props {
}
export interface State {
    value: string
    showAll:boolean
}

/**
 * disable-styleguide
 */
class IconFinder extends React.PureComponent<Props, State> {

    private readonly name = "muk-icon-finder";

    public static defaultProps: Props = {

    }

    state = {
        value: "",
        showAll: false,
    }

    handleChange = (evt) => {
        this.setState({
           value: evt,
        });
    }

    protected renderIcons() {
        const find = this.state.value.toLowerCase().trim()

        const icons = list

        const found = icons.filter( (icon) => {
            if(icon.toLowerCase().indexOf(find) > -1) {
                return true
            }
            return false
        })

        if(found.length === 0) {
            return <p style={{padding: "10px"}}>no icon found :(</p>
        }
        return found.map( obj => {
            return (<Image key={obj} className={`${this.name}__icon`} title={obj} tabIndex={1}>
                        <Icon name={obj} size={30}
                            text={`<Icon type="${camelToKebab(obj)}" />`}
                            style={{display: "inline-block", margin: "5px" }} />
                    </Image>)
        })
    }

    toggleShowAll = () => this.setState(s => ({ value: "", showAll: !s.showAll }))

    public render() {
        const className = `${this.name}`

        return (
            <div className={`${className}`}>
                <Grid cols={"1fr auto"} gap="20px">
                    <TextInput
                        label=""
                        placeholder="Type icon name here..."
                        onChange={this.handleChange}
                        value={this.state.value} />
                    <Button onClick={this.toggleShowAll}>Show all icons</Button>
                </Grid>
                {this.state.showAll || (this.state.value && this.state.value.length > 1) ?
                    this.renderIcons()
                    :
                    false
                }
            </div>
        )
    }
}

const camelToKebab = (string) => {
    return string.replace('Icon','')
            .replace(/([a-z0-9])([A-Z])/g, '$1-$2')
            .replace(/([A-Z])([A-Z])(?=[a-z])/g, '$1-$2')
            .toLowerCase()
}

const Image = css("span")({
    cursor: "pointer",
    position: "relative",
    textAlign: "left",
    ":focus": {
        outline: "none",
    },
    ":focus:before": {
        content: "attr(title)",
        position: "absolute",
        left: "50%",
        top: "-63px",
        transform: "translateX(-50%)",
        whiteSpace: "nowrap",
        background: "#888",
        color: "white",
        zIndex: 1000,
        borderRadius: "2px",
        fontSize: "12px",
        padding: "5px",
    },

    ":focus .muk-icon": {
        zIndex: 1000,
    },
    ":focus .muk-icon__text": {
        display: "inline-block",
    },
    ":focus img": {
        outline: "1px solid #888",
        transform: "scale3d(3,3,1)",
        background: "#fff",
    },

    " .muk-icon": {
        padding: "4px",
        position: "relative",
        border: "1px solid transparent",
    },
    " .muk-icon:hover": {
        background: "white",
        border: "1px solid #ccc",
    },
    " img": {
        transition: "transform 0.2s",
        willChange: "transform",
    },
    " .muk-icon__text": {
        borderRadius: "2px",
        position: "absolute",
        display: "none",
        background: "white",
        color: "#333",
        border: "2px solid #888",
        fontSize: "10px",
        padding: "5px",
        whiteSpace: "nowrap",
        left: "50%",
        top: "68px",
        transform: "translateX(-50%)",
    },
})

export default IconFinder
