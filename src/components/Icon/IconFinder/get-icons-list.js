var fs = require('fs');
var components = {};
var types = {};
var _ = require('underscore');
var glob = require('glob');
var path = require('path');
var rootDir = path.join(__dirname, '../../../../../app-cdn/files/muk/icons');
var attrs = ['xlink:href', 'clip-path', 'fill-opacity', 'fill'];
var cleanAtrributes = function($el, $) {
    _.each(attrs, function(attr) {
        $el.removeAttr(attr);
    });
    if($el.children().length === 0) {
        return false;
    }

    $el.children().each(function(index, el) {
        cleanAtrributes($(el), $);
    });
};
glob(rootDir + '/*.svg', function(err, icons) {
    icons.forEach(function(iconPath){
        var id = path.basename(iconPath, '.svg');
        var svg = fs.readFileSync(iconPath, 'utf-8');
      
        // var folder = iconPath.replace(path.join(rootDir, 'icons') + '/', '').replace( '/' + path.basename(iconPath), '');
        // if (!types[folder]) {
        //     types[folder] = {};
        // }
        types[id] = id;


        // var destination = path.join(rootDir, 'icons/', name + '.tsx')
        // if (!fs.existsSync(path.join(rootDir, 'icons/'))){
        //     fs.mkdirSync(path.join(rootDir, 'icons/'));
        // }
        // fs.writeFileSync(path.join(destination), component, 'utf-8');
        // console.log(destination)
    });

    console.log(types)
    var list = _.map(Object.keys(types), function(id){
        return `    '${id}',`;
    }).join('\n');

    var component = `
export const list = [
${list}
]
`
    console.log(component)
    fs.writeFileSync(path.join('./', 'list.ts'), component, 'utf-8');

});
