import * as React from "react"
import css from "../../../css"
import {Field, IField} from "../Field"
import {styles,stylesProps} from "../TextInput/style"
import InputContainer from "../Field/InputContainer"
import ReactTextareaAutocomplete from "@webscopeio/react-textarea-autocomplete"

export interface Props extends IField {
    height?: number
    multiline?: boolean
}

export class Textarea extends React.Component<Props, {}> {

    protected readonly name = "muk-textarea";

    shouldComponentUpdate(nextProps, nextState) {
        if(
            this.props.error !== nextProps.error ||
            this.props.value !== nextProps.value
        ) {
            return true
        }
        return false
    }


    handleChange = (e) => {
        const {multiline} = this.props
        const value = multiline ? (e.target.value ? e.target.value.split('\n') : []) : (e.target.value ? e.target.value : "")
        if(this.props.setValue) {
            this.props.setValue(value)
        }
        if(this.props.onChange) {
            this.props.onChange(value)
        }
    }

    public render() {
        const { name, label, setValue, invalid, height, children, value, className, autocomplete, ...props } = this.props



        return (
            <Field {...this.props} name={this.name}>

                {autocomplete ? <StyledAutocomplete
                                    loadingComponent={() => <span>...</span>}
                                    minChar={0}
                                    trigger={autocomplete ?? {}}
                                    {...props}
                                    onChange={this.handleChange}
                                    aria-invalid={invalid || this.props.error ? 1 : 0}
                                    height={height || 100}
                                    data-name={name}
                                    value={value}
                                    className={`${this.name}__input
                                        muk-form__input--text
                                        muk-form__input--textarea
                                        ${className}
                                    `}
                    />
            :
                <StyledTextarea
                    {...props}
                    data-name={name}
                    height={height || 100}
                    aria-invalid={invalid || this.props.error ? 1 : 0}
                    className={`${this.name}__input
                                muk-form__input--text
                                muk-form__input--textarea
                                ${className}
                    `}
                    onChange={this.handleChange}
                >{value}</StyledTextarea>
            }
            </Field>
                                                        
        )
    }
}

const StyledAutocomplete = css(ReactTextareaAutocomplete)(styles, (props) => {
    const styles = stylesProps(props)
    return {
        ...styles,
        padding: "8px 10px",
        lineHeight: "1.7em",
        height: `${props.height}px`
    }
})

const StyledTextarea = css("textarea")(styles, (props) => {
    const styles = stylesProps(props)
    return {
        ...styles,
        padding: "8px 10px",
        lineHeight: "1.7em",
        height: `${props.height}px`
    }
})

export default InputContainer(Textarea) as typeof Textarea
