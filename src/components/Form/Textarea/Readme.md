Autocomplete

    <Textarea autocomplete={{
        "%": {
          dataProvider: token => {
            return [
              { name: "%PRVNI%", char: "%PRVNI%" },
              { name: "%DRUHY%", char: "%DRUHY%" }
            ];
          },
          component: ({ entity: { name, char } }) => <div>{`${name}`}</div>,
          output: (item, trigger) => item.char
        }
      }} name="form.textarea1" label={"Write something:"} placeholder={"Try autocomplete on `%` key"} multiline={true}  />


Example

    <Textarea name="form.textarea1" label={"Write something:"} placeholder={"Nothing to see here"} multiline={true}  />

    <Textarea error="Error!" name="form.textarea2" label={"Write something:"} placeholder={"Nothing to see here"} />

    <Textarea invalid={true} name="form.textarea3" label={"Write something:"} placeholder={"Nothing to see here"} />

Uncontrolled

    <Textarea onChange={(value) => alert(value)} value="Sometext" label={"Write something:"} placeholder={"Nothing to see here"} />
