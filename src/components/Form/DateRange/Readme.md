Example

    <DateRange labelFrom="From:" labelTo="To:" nameFrom="form.datefrom" nameTo="form.dateTo" />

Small

    <DateRange labelFrom="From:" labelTo="To:"
        small={true} getter={(from, to) => console.log(from, to) } />

Datetime

    <DateRange labelFrom="From:" labelTo="To:" datetime={true} omitSeconds={true}
         getter={(from, to) => console.log(from, to) } />
