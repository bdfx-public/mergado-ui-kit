import * as React from 'react'
import { DatePicker } from "../DatePicker"
import ConnectedPicker from "../DatePicker"
import Grid from "../../Grid"
import GridCell from "../../GridCell"
import moment from "dayjs"
import css from "../../../css"

interface Props {
    getter?: (dateFrom: string, dateTo: string) => void
    disabledDaysFrom?: Function
    disabledDaysTo?: Function
    dateFrom?: string
    dateTo?: string
    style?: any
    small?: boolean

    labelFrom?: string
    labelTo?: string

    placeholderFrom?: string
    placeholderTo?: string

    inputProps?: any
    className?: string

    showOnMount?: boolean
    showDivider?: boolean

    onChangeFrom?: (value) => void
    onChangeTo?: (value) => void
    nameFrom?: string
    nameTo?: string
    onClearFrom?: () => void
    onClearTo?: () => void
    onClick?: (e: any) => void

    inputFromRef?: any
    readOnly?: boolean

    datetime?: boolean
    omitSeconds?: boolean


    preRender?: DatePicker['props']['preRender']
    postRender?: DatePicker['props']['postRender']

    preRenderFrom?: DatePicker['props']['preRender']
    postRenderFrom?: DatePicker['props']['postRender']

    preRenderTo?: DatePicker['props']['preRender']
    postRenderTo?: DatePicker['props']['postRender']
}

interface State {
    dateFrom: string
    dateTo: string
    enteredFrom: string
    enteredTo: string
}

export class DateRange extends React.PureComponent<Props, State> {

    public inputFrom: any = null
    public inputTo: any = null

    private format = "YYYY-MM-DD"
    constructor(props) {
        super(props)
        this.state = {
            dateFrom: props.dateFrom || null,
            dateTo: props.dateTo || null,
            enteredFrom: null,
            enteredTo: null
        }

        this.inputFrom = props.inputFromRef || React.createRef();
        this.inputTo = React.createRef();

        if(props.datetime) {
            this.format = props.omitSeconds ? "YYYY-MM-DD HH:mm" : "YYYY-MM-DD HH:mm:ss"
        }
    }

    componentDidMount() {
        if(this.props.showOnMount) {
            this.inputFrom.current.focus()
        }
    }

    componentDidUpdate(prevProps) {
            if(this.props.dateFrom !== prevProps.dateFrom) {
                this.setState({ dateFrom: this.props.dateFrom })
            }

            if(this.props.dateTo !== prevProps.dateTo) {
                this.setState({ dateTo: this.props.dateTo })
            }
    }

    handleDayMouseEnterStart = (day) => {
        const { dateFrom, dateTo } = this.state
        // if (!this.isSelectingFirstDay(dateFrom, dateTo, day)) {
            this.setState({
                enteredFrom: day,
            })
        // }
    }

     handleDayMouseEnterEnd = (day) => {
        const { dateFrom, dateTo } = this.state
        // if (!this.isSelectingFirstDay(dateFrom, dateTo, day)) {
            this.setState({
                enteredTo: day,
            })
        // }
    }

    resetSelected = () => setTimeout(() => {
        this.setState({
            enteredFrom: this.state.dateFrom,
            enteredTo: this.state.dateTo,
        })},100)


    onChangeDateFrom = (value, changed) => {
        if(this.props.onChangeFrom) {
            this.props.onChangeFrom(moment(value).format(this.format))
        }
        this.getter(moment(value).format(this.format),this.state.dateTo);
        const focus = changed === "date" ? this.focusDateTo : undefined
        this.setState({
            dateFrom: moment(value).format(this.format),
        }, focus)
    }

    onChangeDateTo = e => {
        if(this.props.onChangeTo) {
            this.props.onChangeTo(moment(e).format(this.format))
        }
        this.getter(this.state.dateFrom, moment(e).format(this.format));

        this.setState({
            dateTo: moment(e).format(this.format),
        })
    }

    getter = (from, to) => {
        if(from && to && this.props.getter) {
            this.props.getter(from, to)
        }
    }

    filterDays = date => date > moment() || date < moment('2018-01-01')

    filterDaysFrom = date => this.props.disabledDaysFrom ? this.props.disabledDaysFrom(date) : this.state.dateTo && moment(date) > moment(this.state.dateTo).add(1,'day')

    filterDaysTo = date => this.props.disabledDaysTo ? this.props.disabledDaysTo(date) : this.state.dateFrom && moment(date) < moment(this.state.dateFrom)

    focusDateTo = () => {
        this.inputTo.current.focus()
    }

    render() {
        const { style, small,className, showDivider, nameFrom, nameTo, onClearFrom, onClearTo, datetime, omitSeconds, } = this.props
        const { dateFrom, dateTo, enteredFrom, enteredTo } = this.state
        const from = dateFrom ? moment(dateFrom).toDate() : null
        const to = dateTo ? moment(dateTo).toDate() : null
        const start = enteredFrom ? moment(enteredFrom).toDate() : null
        const end = enteredTo ? moment(enteredTo).toDate() : null
        const pickerProps = {   showOverlay: true, showOutsideDays: true, className: "Range",
                                modifiers: { start: from, end: to, selectstart: start, selectend: end },
                                selectedDays: [start, { from: start, to: end }],
                                numberOfMonths: 2,
                            }

        const pickerPropsFrom = {
            ...pickerProps,
            disabledDays: this.filterDaysFrom,
            toMonth: to,
        }
        const pickerPropsTo = {
            ...pickerProps,
            disabledDays: this.filterDaysTo,
            month: from,
            fromMonth: from,
        }

        const Picker = nameFrom && nameTo ? ConnectedPicker : DatePicker

        return (
            <Wrapper className={`muk-daterange ${className}`} cols={`1fr ${showDivider ? "auto" : ""} 1fr`} style={style} gap={"10px"} small={small}>
                <GridCell valign={"center"}>
                    <Picker
                        className="muk-picker-from"
                        name={nameFrom}
                        inputRef={this.inputFrom}
                        pickerProps={pickerPropsFrom}
                        placeholder={this.props.placeholderFrom}
                        onClear={onClearFrom}
                        label={this.props.labelFrom}
                        value={dateFrom !== null ? moment(dateFrom).format(this.format) : null}
                        onChange={this.onChangeDateFrom}
                        onDayMouseEnter={this.handleDayMouseEnterStart}
                        onFocus={this.resetSelected}
                        small={small}
                        datetime={datetime}
                        omitSeconds={omitSeconds}
                        preRender={this.props.preRenderFrom ?? this.props.preRender}
                        postRender={this.props.postRenderFrom ?? this.props.postRender}
                        />
                </GridCell>
                {showDivider &&
                <GridCell className="m-divider" valign="center" style={{marginTop: this.props.labelFrom ? "25px" : undefined}}>
                    —
                </GridCell>
                }
                <GridCell valign={"center"}>
                    <Picker
                        inputRef={this.inputTo}
                        className="muk-picker-to"
                        name={nameTo}
                        pickerProps={pickerPropsTo}
                        placeholder={this.props.placeholderTo}
                        label={this.props.labelTo}
                        onClear={onClearTo}
                        value={dateTo !== null ? moment(dateTo).format(this.format) : null}
                        onChange={this.onChangeDateTo}
                        onDayMouseEnter={this.handleDayMouseEnterEnd}
                        onFocus={this.resetSelected}
                        small={small}
                        datetime={datetime}
                        omitSeconds={omitSeconds}
                        preRender={this.props.preRenderTo ?? this.props.preRender}
                        postRender={this.props.postRenderTo ?? this.props.postRender}
                        />
                </GridCell>
            </Wrapper>
        )
    }
}
const Wrapper = css(Grid)({}, props => ({

    " .muk-picker-to.muk-datepicker .muk-datepicker-popover": {
        right: "0 !important",
        left: "initial !important",
    },

    " .DayPicker .DayPicker-Months": {
        display: "grid !important",
        gridTemplateColumns: "1fr 1fr !important",
    },

    " .DayPicker-Day--selectstart:not(.DayPicker-Day--disabled)": {
        backgroundColor: 'rgba(81,150,250,0.6)',
        borderTopLeftRadius: "100%",
        borderBottomLeftRadius: "100%",
    },

    " .DayPicker-Day--selectend:not(.DayPicker-Day--disabled)": {
        backgroundColor: 'rgba(81,150,250,0.6)',
        borderTopRightRadius: "100%",
        borderBottomRightRadius: "100%",

    },


    " .DayPicker-Day--start:not(.DayPicker-Day--outside)": {
    backgroundColor: 'rgba(81,150,250,1) !important',
    },

    " .DayPicker-Day--end:not(.DayPicker-Day--outside)": {
    backgroundColor: 'rgba(81,150,250,1) !important',
    },

    " .DayPicker-Day": {
        padding: props.small ? "4px 8px !important" : undefined,
    },

    " .muk-datepicker-popover": {
        fontSize: props.small ? "11px" : "14px",
    },
}))

export default DateRange
