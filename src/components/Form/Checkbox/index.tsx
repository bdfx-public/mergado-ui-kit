import * as React from "react"
import css from "../../../css"
import Span from "../../Span"
import Icon from "../../Icon"
import {Field, IField, } from "../Field"
import InputContainer from "../Field/InputContainer"
import Div from "../../Div"
import FieldError from "../FieldError"
import Theme from "../../Theme"
import Input from "./input"

export interface Props extends IField {
    checked?: boolean
    halfway?: boolean
    dataId?: string | number

    handleChange?: Function
}

export class Checkbox extends React.Component<Props, {}> {
    protected readonly name = "muk-input-checkbox"

    shouldComponentUpdate(nextProps, nextState) {
        if(
            this.props.value !== nextProps.value ||
            this.props.error !== nextProps.error ||
            this.props.checked !== nextProps.checked
        ) {
            return true
        }
        return false
    }

    handleChange = (evt) => {
        const value = evt.target ? evt.target.checked : evt
        if(this.props.setValue) {
            this.props.setValue(value ? 1 : null)
        }
        if(this.props.onChange) {
            this.props.onChange(value ? 1 : 0)
        }
    }

    protected renderLabel() {
        const { label, dataId, value, checked, setValue, ...props } = this.props
        return <>
                <Label className={`m-isinvalid ${this.props.error ? 'm-invalid' : ""}`}>
                    <Input handleChange={this.handleChange} {...this.props} />
                    <Span className={`m-label-wrapper`} fontSize={"16px"} fontWeight={"normal"}>
                        {label && " " }{label}
                    </Span>
                </Label>
                <FieldError error={this.props.error} className={`muk-form__validation`} style={{marginTop: "-1px"}} />
            </>

    }

    renderError = () => <FieldError error={this.props.error} className={`muk-form__validation`} />

    public render() {
        return <Field className={`muk-checkbox  ${!this.props.label ? "m-no-label" : ""}`} {...this.props} invalid={false} error={null} label={this.renderLabel()} />
    }
}


const Label = css("div")({
    cursor: "pointer",

    "&:hover .muk-checkbox-input": {
        borderColor: Theme.blue,
    },

    "&.m-no-label": {
        height: "16px !important",
    },

})

export default InputContainer(Checkbox) as typeof Checkbox
