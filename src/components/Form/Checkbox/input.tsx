import * as React from "react"
import css from "../../../css"
import Icon from "../../Icon"
import {IField, } from "../Field"
import Div from "../../Div"
import Theme from "../../Theme"

export interface Props extends IField {
    checked?: boolean
    halfway?: boolean
    dataId?: string | number

    handleChange?: Function
}
export const Input = ({dataId, checked, value, label, handleChange, ...props}: Props) => {
    const active = checked !== undefined ? checked : !!value
    return (
    <Div className="m-element-wrapper" lineHeight={"16px"} position="relative" display="inline-block" verticalAlign="text-bottom">
                        <InputCheckbox
                            className={`m-item`}
                            {...props}
                            data-id={dataId}
                            data-name={name}
                            onChange={handleChange}
                            checked={active}
                            type="checkbox"
                            style={{display: "none !important"}}
                            />
                        <StyledInput className={"muk-checkbox-input"} label={label}>
                            <Icon color={active ? "white" : "blue"} type="check-slim" className="m-check muk-icon--check" size={16} />
                        </StyledInput>
                    </Div>
    )
}


const styles = {
    ":checked + .muk-checkbox-input .muk-icon--check": {
        opacity: 1,
    },
    ":checked + .muk-checkbox-input": {
        borderColor: `${Theme.blue}`,
        background: `${Theme.blue}`,
    },
}


const InputCheckbox = css("input")(styles)

export const StyledInput = css("span")({
    display: "inline-block",
    background: "transparent",
    width: "18px",
    height: "18px",
    position: "relative",
    textAlign: "center",
    transition: "border-color 0.2s",
    "& .muk-icon--check": {
        opacity: 0,
    },
    borderRadius: `${Theme.radius}`,
    border: `1px solid ${Theme.decoration}`,
    ":hover": {
        borderColor: `${Theme.blue}`,
    },
}, (props: any) => {
    return {
        marginRight: props.label ? "5px" : "0px",
    }
})

export default Input
