import * as React from "react"
import css from "../../../css"
import FieldLabel from "../FieldLabel"
import FieldError from "../FieldError"
import Theme from "../../Theme"

export interface IField {
    name?: string
    value?: string | number | boolean | (number|string|boolean)[]
    invalid?: boolean
    error?: string
    style?: any
    className?: string
    id?: string
    checked?: boolean
    inputRef?: any

    label?: string | JSX.Element
    placeholder?: string
    title?: string
    bigLabel?: boolean

    readOnly?: boolean
    disabled?: boolean
    required?: boolean

    onChange?: (value,evt?) => void
    onClick?: (evt) => void
    onClear?: () => void
    setValue?: (value) => void
    formatter?: (value) => any

    renderError?: () => any

    [propName: string]: any
}

export const Field: React.SFC<IField> = (props) => {
    const { style, error, renderError, ...others } = props
    const isInvalid = props.invalid || error
    return (
        <FieldWrapper className={`muk-form-group ${props.name ? "m-field-" + props.name : ""} ${props.disabled ? "disabled" : ""} ${props.required ? "required" : ""}  ${props.className || ""}`}
            title={props.title}
            name={props.name}
            style={style}>
                <FieldError error={error} className={`muk-form__validation`} />
                {props.label &&
                    <FieldLabel name={props.name} bigLabel={props.bigLabel}>
                        {props.label}
                    </FieldLabel>
                }
                <div className={`m-isinvalid ${isInvalid ? `m-invalid` : ""}`}>
                    {props.children}
                </div>
        </FieldWrapper>
    )
}

const FieldWrapper = css("div")({
    position: "relative",

    "input:invalid": {
        background: "#fff",
    },

    "&.m-no-label.muk-checkbox": {
        "label": {
            height: "18px",
        },
        ".muk-form-label > .m-isinvalid": {
            height: "18px",
        },
    },
},(props: any) => {
    let styles = {}
    if(props.name) {
        styles = {
            marginBottom: "30px",
            paddingRight: "10px",
        }
    }

    return {
        ...styles,

        "& .m-isinvalid.m-invalid *": {
            borderColor: `${Theme.red} !important`,
        },
    }
})

export default Field
