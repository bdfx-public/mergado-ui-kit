import {connect} from "react-redux";
import dotProp from "dot-prop-immutable";
const UPDATE_INPUT_VALUE = 'UPDATE_INPUT_VALUE';
const INIT_FORM = 'INIT_FORM';
export const changeField = (type, name, value) => ({
    type,
    name,
    value,
});

const getActionObj = (type, name, value) => ({
    type,
    payload: {
        name,
        value,
    },
})

export const createBoundType = namespace =>
    (UPDATE_INPUT_VALUE + '.' + namespace);

export const duck = (namespace, defaultState, onStateChange, actionHandlers) =>
    (state = defaultState || {}, action) => {
        const boundType = createBoundType(namespace);

        const reducer = {
            [INIT_FORM]: (s,a) => {
                if(namespace === a.payload.form) {
                    return a.payload.data;
                }
                return s;
            },
            [boundType]: () => {
                const fieldPathWithoutNamespace = action.name.replace(namespace + '.', '');
                const newState = dotProp.set(state, fieldPathWithoutNamespace, action.value);
                return onStateChange && onStateChange(newState) || newState;
            },

            ...actionHandlers,
        };

        return reducer[action.type]
            ? reducer[action.type](state, action)
            : state;
    };

const getFieldNamespace = nameProp =>
  (nameProp.split('.')[0]);

const w: any = window

const dispatcher = (state, dispatch, own) => {
    if(!own.name) {
        return own
    }
    let value: string | string[] | number | number[] = ''
    if(own.name) {
        value = dotProp.get(state, w._ENABLED_UPDATE_VALUE ? 'form.'+own.name : own.name)
    }
    if(own.type === "radio") {
        value = own.value
    }
    if(own.selectItem) {
        value = value ? `${value}`.split('|') : []
    }
    if(own.multiline) {
        value = value && Array.isArray(value)  ? value.join('\n') : value
    }
    let error: string = own.error
    if(own.errName && own.err) {
        const formName = own.name.split('.')[0]
        if(state.form[formName]?.errors?.[own.errName]) {
            error = own.err(state.form[formName].errors[own.errName])
        }
    }

    return ({
            ...own,
            error,
            value,
            setValue: (val) => {
                if(w._ENABLED_UPDATE_VALUE) {
                    dispatch.dispatch(getActionObj('UPDATE_VALUE', own.name, val))
                } else {
                    dispatch.dispatch(changeField(createBoundType(getFieldNamespace(own.name)), own.name, val))
                }
            },
        })
}

const InputContainer: (component) => typeof component = (component) => {
    return connect(
                state => state,
                dispatch => ({
                    dispatch,
                }),
                dispatcher,
            )(component)
}

export default InputContainer;
