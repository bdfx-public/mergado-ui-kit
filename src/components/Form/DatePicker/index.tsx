import * as React from "react"
import css from "../../../css"
import InputContainer from "../Field/InputContainer"
import {Field, IField} from "../Field"
import ReactDatePicker from "react-day-picker"
import dayjs from "dayjs"
import {TextInput} from "../TextInput"
import Button from "../../Button"
import Theme from "../../Theme"
import Grid from "../../Grid"
import GridCell from "../../GridCell"
import FieldLabel from "../FieldLabel"
import {style as factoryStyle} from "./style"
import {styles as inputStyles, stylesProps as inputStylesProps} from "../TextInput/style"
import ReactDOM from "react-dom"
import Icon from "../../Icon"

interface Props extends IField {
    value?: string
    locale?: "cs" | "sk"
    pickerProps?: any
    datetime?: boolean
    onDayMouseEnter?: Function
    inputRef?: any
    onFocus?: any
    omitSeconds?: boolean

    onClear?: () => void
    trans?: Function

    small?: boolean

    preRender?: (props: any, state: any, handleHide: Function) => any | JSX.Element
    postRender?: (props: any, state: any, handleHide: Function) => any | JSX.Element
}

interface State {
    startDate: string
    startTime: string
    showPicker: boolean
    manual: string
    invalid: boolean
}

export class DatePicker extends React.Component<Props, State> {

    protected readonly name = "muk-datepicker"
    protected locale;

    constructor(props) {
        super(props)
        const startDate = props.value ? dayjs(props.value).format('YYYY-MM-DD') : null
        const startTime = props.value ? dayjs(props.value).format('HH:mm:ss') : null

        this.state = {
            showPicker: false,
            startDate,
            startTime,
            manual: null,
            invalid: false,
        }
        const locale = dayjs().locale()
        if(locale === "cs") {
            import('dayjs/locale/cs').then(e => {
                this.locale = e.default
            })
        } else
        if(locale === "sk") {
            import('dayjs/locale/sk').then(e => {
                this.locale = e.default
            })
        } else
        if(locale === "pl") {
            import('dayjs/locale/pl').then(e => {
                this.locale = e.default
            })
        } else {
            import('dayjs/locale/en').then(e => {
                this.locale = e.default
            })
        }
    }

    myRef = React.createRef();

    shouldComponentUpdate(nextProps, nextState) {
        if(
            this.props.value !== nextProps.value ||
            this.props.error !== nextProps.error ||
            this.state.manual !== nextState.manual ||
            this.state.showPicker !== nextState.showPicker ||
            this.state.startDate !== nextState.startDate ||
            this.state.startTime !== nextState.startTime ||
            ( this.props.pickerProps && this.props.pickerProps.selectedDays &&
                this.props.pickerProps.selectedDays[1] &&
                this.props.pickerProps.selectedDays[1].to !== nextProps.pickerProps.selectedDays[1].to
            )
        ) {
            return true
        }
        return false
    }

    componentDidUpdate(prevProps, prevState) {
        if(this.state.manual === null) {
            const startDate = this.props.value ? dayjs(this.props.value).format('YYYY-MM-DD') : null
            const startTime = this.props.value ? dayjs(this.props.value).format('HH:mm:ss') : null

            this.setState({
                startDate,
                startTime,
                invalid: false,
            })
        }

        if(this.state.showPicker && !prevState.showPicker) {
            const n:any = ReactDOM.findDOMNode(this.refs['popover'])
            const c = n.getBoundingClientRect()
            const avail = Math.max(document.documentElement.clientWidth, window.innerWidth || 0)
            if (c.x + c.width + 5 > avail) {
                n.style.right = 0
                n.style.left = 'initial'
            } else {
                n.style.left = 0
                n.style.right = 'initial'
            }
        }

    }

    handleDateChanged = (date) => {
        if(!dayjs(date).isValid()) {
            return
        }
        const set =  `${dayjs(date).format("YYYY-MM-DD")}${this.props.datetime ? ` ${(this.state.startTime || "00:00:00")}` : ""}`
        this.setState({ startDate: dayjs(set).format("YYYY-MM-DD"), showPicker: false, manual: null, })

        if(this.props.setValue) {
            this.props.setValue(set)
        }
        if(this.props.onChange) {
            return this.props.onChange(set, "date")
        }
    }

    handleChanged = (e) => {
        const d = e.split(' ')
        const date = dayjs(`${d[1] || ""} ${d[0] || ""} ${d[2] || ""} ${d[3] || ""}`);
        const invalid = !date.isValid()
        if(invalid) {
            this.setState({
                manual: e,
                invalid,
            })
        } else {
            const set =  `${date.format(this.props.datetime ? "YYYY-MM-DD HH:mm:ss" : "YYYY-MM-DD")}`
            this.setState({ startDate: date.format("YYYY-MM-DD"),
                            startTime: date.format("HH:mm:ss"),
            })

            if(this.props.setValue) {
                this.props.setValue(set)
            }
            if(this.props.onChange) {
                this.props.onChange(set)
            }
            this.setState({
                manual: e,
                invalid: false,
            })
        }
    }


    handleTimeChanged = (type) => (value, operation = "set") => {

        const startDate = this.state.startDate ?  this.state.startDate : dayjs().format("YYYY-MM-DD")
        let set = dayjs(`${startDate} ${this.state.startTime || "00:00:00"}`)
        if(operation === "add") {
            set = set.add(value,type)
        } else
        if(operation === "subtract") {
            set = set.subtract(value,type)
        } else {
            set = set.set(type, value)
        }

        if(!set.isValid()) {
            return false
        }


        this.setState({ startTime: set.format("HH:mm:ss"), manual: null, })
        if(this.props.setValue) {
            this.props.setValue(set.format(this.props.datetime ? "YYYY-MM-DD HH:mm:ss" : "YYYY-MM-DD"))
        }
        if(this.props.onChange) {
            return this.props.onChange(set.format(this.props.datetime ? "YYYY-MM-DD HH:mm:ss" : "YYYY-MM-DD"), "time")
        }
    }

    handleClick = () => { this.setState({showPicker: true}); if(this.props.onFocus) { this.props.onFocus() } }
    handleHide = () => this.setState({showPicker: false})



    add = (type) => (e) => this.handleTimeChanged(type)(1,'add')
    subtract = (type) => (e) => this.handleTimeChanged(type)(1,'subtract')

    onClear = () => {
        if(this.props.onClear) {
            this.props.onClear()
        }
        if(this.props.setValue) {
            this.props.setValue(null)
        }
        if(this.props.onChange) {
            this.props.onChange(null)
        }
        this.setState({
                startDate: null,
                startTime: null,
        })
    }

    public render() {
        const { trans, preRender, postRender, className, label, name, placeholder, onDayMouseEnter, onFocus, value, onChange, locale, children, pickerProps, datetime, setValue, ...props } = this.props
        const {showPicker, manual, invalid} = this.state

        const startTime = this.state.startDate ?  dayjs(this.state.startDate + " " + this.state.startTime) : dayjs().startOf('day')
        const FORMAT = datetime ? (props.omitSeconds ?  "DD. MM. YYYY HH:mm" : "DD. MM. YYYY HH:mm:ss") : "DD. MM. YYYY"
        return(
            <StyledField className={`${className || ""} muk-datepicker`}>
                <div>
                    {/* visible */}
                    <TextInput {...props}
                            onFocus={this.handleClick}
                            type={this.props.onClear || name ? "search" : "text"}
                            onClear={this.props.onClear || name ? this.onClear : undefined}
                            label={label}
                            inputRef={this.props.inputRef}
                            style={{
                                " input": {
                                    background: invalid ? "rgba(255,0,0,0.2)" : undefined,
                                }
                            }}
                            data-name={name}
                            onChange={this.handleChanged}
                            placeholder={placeholder || (this.state.startDate ? dayjs(this.state.startDate + " " + this.state.startTime).format(FORMAT) : FORMAT)}
                            value={manual ? manual : this.state.startDate ? dayjs(this.state.startDate + " " + this.state.startTime).format(FORMAT) : ""} />
                </div>
                {showPicker &&
                <Popover className="muk-datepicker-popover" ref={"popover"}>
                    <Cover onClick={this.handleHide} className="muk-datepicker-cover" />
                    <Picker className="muk-datepicker-picker">

                    {preRender && (typeof preRender === "function" ? preRender(this.props, this.state,this.handleHide) : preRender)}

                    <Grid valign="center" cols={datetime ? "1fr 1fr" : "1fr"}>
                    {!!datetime &&
                    <FieldLabel className="muk-datepicker-pickerlabel label-time">{trans ? trans("time") : "Čas"}:</FieldLabel>
                    }
                    <FieldLabel className="muk-datepicker-pickerlabel">{trans ? trans("day") : "Den"}:</FieldLabel>

                    {!!datetime &&
                    <div className="time">
                        <Grid className="datetime-wrapper" cols={`auto auto auto ${!this.props.omitSeconds ? "auto auto" : ""}`}>
                            <Button className="muk-addsubtract-button" onClick={this.add('hour')} icon={<Icon type="chevron-up" />} color="nocolor" />
                            <GridCell />
                            <Button className="muk-addsubtract-button"  onClick={this.add('minute')} icon={<Icon type="chevron-up" />} color="nocolor" />
                        {!this.props.omitSeconds &&
                        <>
                            <GridCell />
                            <Button className="muk-addsubtract-button"  onClick={this.add('second')} icon={<Icon type="chevron-up" />} color="nocolor"  />
                        </>}
                            <TextInput value={startTime.hour() + ""}
                                    className="muk-datepicker-inputtime"
                                step={1}
                                label={trans ? trans("hour") : "Hodin"}
                                onChange={this.handleTimeChanged('hour')} />
                            <div className="center-gridcell">
                            :
                            </div>
                            <TextInput value={startTime.minute() + ""}
                                className="muk-datepicker-inputtime"
                                step={1}
                                label={trans ? trans("minute") : "Minut"}
                                onChange={this.handleTimeChanged('minute')} />
                        {!this.props.omitSeconds &&
                        <>
                            <div className="center-gridcell">
                            :
                            </div>
                            <TextInput value={startTime.second() + ""}
                                className="muk-datepicker-inputtime"
                                label={trans ? trans("second") : "Sekund"}
                                step={1}
                                onChange={this.handleTimeChanged('second')} />
                                </>
                        }
                            <Button  className="muk-addsubtract-button" onClick={this.subtract('hour')} icon={<Icon type="chevron-down" />} color="nocolor" />
                            <GridCell />
                            <Button className="muk-addsubtract-button" onClick={this.subtract('minute')} icon={<Icon type="chevron-down" />} color="nocolor" />
                        {!this.props.omitSeconds &&
                        <>
                            <GridCell />
                            <Button className="muk-addsubtract-button" onClick={this.subtract('second')} icon={<Icon type="chevron-down" />} color="nocolor"  />
                        </>
                        }

                        </Grid>
                    </div>
                    }
                    <div>
                        <ReactDatePicker
                            onDayClick={this.handleDateChanged}
                            className="muk-datepicker-reactdatepicker"
                            {...{
                                firstDayOfWeek: 1,
                                months: this.locale.months,
                                weekdaysLong: this.locale.weekdays,
                                weekdaysShort: this.locale.weekdaysShort,
                                selectedDays: this.state.startDate !== null ? [dayjs(this.state.startDate).toDate()] : [],
                                // month: this.state.startDate ?  dayjs(this.state.startDate).toDate() : null,
                                ...pickerProps,
                            }}
                            onDayMouseEnter={onDayMouseEnter}
                            {...props}
                        />
                    </div>
                    </Grid>
                        {postRender && (typeof postRender === "function" ? postRender(this.props, this.state,this.handleHide) : postRender)}
                    </Picker>
                </Popover>
            }
            </StyledField>
        )
    }
}

const Picker = css("div")({
    ...factoryStyle,
    background: "white",
    boxShadow: "1px 2px 5px 0px rgba(122,122,122,0.5)",
    " .muk-form-label": {
        marginLeft: "5px",
    },
    ".label-time": {
        borderRight: "1px solid white",
    },
    ".center-gridcell": {
        marginTop: "32px",
    },
})

const Popover = css("div")({
    position: "absolute",
    zIndex: 200,
    ".datetime-wrapper": {
        maxWidth: "250px",
        margin: "auto",
    },


    " .muk-datepicker-inputtime": {
        " input": {
            textAlign: "center",
            paddingRight: "10px !important",
        },
        ".muk-form-label": {
            textAlign: "center",
            marginLeft: 0,
            marginTop: "-10px",
        },
        width: "80%",
        margin: "auto",
    },

    " .muk-addsubtract-button": {
        cursor: "pointer",
        zIndex: 1000,
    },

    " label.muk-datepicker-pickerlabel": {
        background: Theme.blue,
        color: "white",
        textAlign: "center",
        marginLeft: "0",
        marginTop: "0",
        height: "22px",
    },



    ".muk-datepicker-inputtime": {
        zoom: 0.9,
    },

    " .DayPicker-wrapper": {
        paddingTop: "1em",
    },
})

const Cover = css("div")({
    position: "fixed",
    top: "0px",
    right: "0px",
    bottom: "0px",
    left: "0px",
})

const StyledField = css(Field)({
    display: "block",
    width: "100%",

    " input": {
        ...inputStyles,
    }
}, props => {
    return {
    " input": inputStylesProps(props)
}})

export default InputContainer(DatePicker) as typeof DatePicker
