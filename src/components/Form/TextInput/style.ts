
import React from "react"
import {colord as Color} from "colord"
import Theme from "../../Theme"


export const styles = {
    boxSizing: "border-box",
    backgroundColor: "#fff",
    display: "block",
    textAlign: "left",
    outline: "none",
    color: "#333",
    transition: "border-color 0.2s",
    willChange: "border-color",
    width: "100%",
    borderWidth: "1px",
    paddingRight: "25px !important",
}

export const stylesProps = (props) => {

    const type = (props.type === "file" || props.type === "time") ? {
            lineHeight: "initial",
            padding: "8px !important",
            height: "auto",
        } : {}

    const disabled = props.disabled ? {
            color: "#999",
            background: "#eee",
            borderColor: Color(Theme.grey).alpha(0.5).toRgbString(),
    } : {}

    const size = props.small ? {
        height: "28px !important",
        lineHeight: "25px !important",
        fontSize: "12px !important",
        padding: "0 8px !important",
    } : {
        height: "40px",
        lineHeight: "40px",
        fontSize: "14px",
        padding: "0 10px",
    }

    return {
        ...disabled,
        ...size,
        border: Theme.input_border,
        borderRadius: Theme.radius,
        ...type,
        "&:active": {
            border: `${Theme.input_border_active}`,
        },
        "&:focus": {
            border: `${Theme.input_border_active}`,
        },
    }
}
