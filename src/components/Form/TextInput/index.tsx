import * as React from "react"
import css from "../../../css"
import Div from "../../Div"
import Icon from "../../Icon"
import {Field, IField} from "../Field"
import Button from "../../Button"
import InputContainer from "../Field/InputContainer"
import debounce from "lodash/debounce"
import {styles, stylesProps} from "./style"

export interface Props extends IField {
    value?: number | string
    type?: "text" | "number" | "password" | "hidden" | "email" | "search" | "tel" | "url" | "file" | "time"
    min?: number | string
    max?: number | string
    step?: number | string
    onFocus?: (e) => void
    onBlur?:  (e) => void
    autoComplete?: string
}

interface State {
    passwordVisible: boolean
    value: number | string
}

export class TextInput extends React.Component<Props, State> {

    protected readonly name = "muk-textinput"

    constructor(props) {
        super(props)

        this.state = {
            passwordVisible: false,
            value: props.value ?? "",
        }
    }

    componentDidUpdate(prevProps, prevState) {
        if(prevProps.value !== this.props.value) {
            this.setState({ value: this.props.value })
        }
    }

    shouldComponentUpdate(nextProps, nextState) {
        if(this.state.value !== nextState.value || this.props.value !== nextProps.value ||
            this.props.className !== nextProps.className ||
            this.props.error !== nextProps.error ||
            this.state.passwordVisible !== nextState.passwordVisible) {
            return true
        }
        return false
    }


    setValue = debounce((value) => this.props.setValue(value), 1000)

    formatValue = (value) => {
        let numvalue
        if(this.props.type === "number") {
                numvalue = value ? parseFloat(value) : null;
        } else {
            return value
        }
        return !isNaN(numvalue) ? numvalue : value;
    }

    handleChange = (evt) => {
        const value = this.formatValue(evt.target.value)

        this.setState({ value })

        if(this.props.setValue) {
            this.setValue(value)
        }

        if(this.props.onChange) {
            this.props.onChange(value, evt)
        }
    }

    handleBlur = (evt) => {
        if(this.props.onBlur) {
            this.props.onBlur(evt)
        }
        if(this.props.setValue) {
            const value = this.formatValue(evt.target.value)
            this.props.setValue(value)
        }
    }

    handleClear = () => {
        if(this.props.onClear) {
            this.props.onClear()
        } else
        if(this.props.setValue) {
            this.props.setValue(this.formatValue(""));
        } else
        if(this.props.onChange) {
            this.props.onChange(this.formatValue(""));
        }
    }

    showPassword = () => this.setState({ passwordVisible: true })
    hidePassword = () => this.setState({ passwordVisible: false })

    public render() {
        const { placeholder, children, name, style, inputRef, className, ...props} = this.props
        const type = this.props.type ? this.props.type : "text"
        const isInvalid = props.invalid || props.error
        const { value } = this.state
        return (
            <Field {...props} className={`${this.name} ${className || ""}`} style={style} name={name}>
                <Div className="m-textinput-wrapper" position="relative">
                <CssInput
                    {...props}
                    ref={inputRef}
                    value={value ?? ""}
                    onChange={this.handleChange}
                    placeholder={placeholder}
                    onBlur={this.handleBlur}
                    type={type === "search" || (type === "password" && this.state.passwordVisible)
                                                ? "text" : type}
                    aria-invalid={isInvalid ? 1 : 0}
                    className={`m-textinput-input m-textinput-${type} m-input`}
                    data-name={name}
                />
                {type === "password" && this.state.passwordVisible === false &&
                    <CssButtonEye className="m-openedeye" icon={<Icon type="eye" />}
                            type={"void"}  color="nocolor" size="tiny"
                            onClick={this.showPassword} />
                }
                {type === "password" && this.state.passwordVisible === true &&
                    <CssButtonEye className="m-closedeye" icon={<Icon type="eye-slash" />}
                            type={"void"}  color="nocolor" size="tiny"
                            onClick={this.hidePassword} />
                }{(type === "search" || props.onClear) && !!value &&
                    <button tabIndex={-1} className="react-select-item-clear" onClick={this.handleClear} />
                }
                </Div>
            </Field>
        )
    }
}
const CssInput = css("input")(styles,stylesProps)

const CssButtonEye = css(Button)({
    right: "5px",
    bottom: 0,
    top: 0,
    position: "absolute",
    margin: "auto",
    height: "25px",
})

export default InputContainer(TextInput) as typeof TextInput

