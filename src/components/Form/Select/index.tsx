import * as React from "react"
import css from "../../../css"
import Div from "../../Div"
import Icon from "../../Icon"
import Button from "../../Button"
import {Field,IField} from "../Field"
import {Select as SelectItem} from "react-select-item"
import {styles, stylesProps} from "./style"
import InputContainer from "../Field/InputContainer"
import {CustomOption} from "./custom-option"
import {styles as inputStyles, stylesProps as inputPropsStyles} from "../TextInput/style"

export interface Option {
    value: string
    label: string
    text?: string | JSX.Element
}

interface Props extends IField {
    options: Option[]
    multiple?: boolean
    array?: boolean
    default?: string | string[]
    omitStyles?: boolean
    notClearable?: boolean
    button?: JSX.Element
    renderItem?: (option) => string | JSX.Element
    printItem?: (option) => string | JSX.Element
}

interface State {
}

export class Select extends React.Component<Props, State> {

    protected readonly name = "muk-select";

    shouldComponentUpdate(nextProps, nextState) {
        if(
            this.props.error !== nextProps.error ||
            this.props.value !== nextProps.value
        ) {
            return true
        }
        return false
    }


    onChange = e => {
        const value = this.props.array ? e.filter(o => o) : e.filter(o => o).join('|')
        if(this.props.setValue) {
            this.props.setValue(value)
        }
        if(this.props.onChange) {
            return this.props.onChange(value)
        }
    }

    onClear = () => {
        if(this.props.onClear) {
            this.props.onClear()
        }
        if(this.props.setValue) {
            this.props.setValue(this.props.default || "")
        }
        if(this.props.onChange) {
            this.props.onChange(this.props.default || "")
        }
    }


    renderOption = o => ({ ...o, value: o.value === null ? undefined : o.value, name: o.label, printItem: this.props.printItem, renderItem: this.props.renderItem })

    onSearch = (phrase, option) => {
        return option.label.toLowerCase().indexOf(phrase.toLowerCase()) !== -1;
    }

    public render() {
        const {onChange, notClearable, label, placeholder, multiple, name, omitStyles, button, ...props} = this.props
        const value = this.props.value ? (typeof this.props.value  === "string" ? this.props.value.split("|") : this.props.value) : []
        const isEmpty = !this.props.value || (Array.isArray(this.props.value) && this.props.value.length === 0)
        const Button = button ? () => button : undefined
        const Element = omitStyles || Button !== undefined ? SelectItem : StyledSelect
        return (
            <Wrapper {...this.props} name={this.name}>
                <Div position="relative">
                    <Element
                           closeOnChange={!multiple}
                           search={props.options.length > 6 && typeof value !== "number"}
                           placeholder={placeholder ?? "- - -"}
                           searchEmptyPlaceholder={""}
                           searchPlaceholder={""}
                           clearText={""}
                           LabelComponent={Button}
                           OptionComponent={CustomOption}
                           onSearch={this.onSearch}
                           {...props}
                           multiple={multiple}
                           className={`m-select ${multiple ? "m-select-multiple" : ""} ${props.className || ""}`}
                           value={value}
                           onChange={this.onChange}
                           options={props.options.map(this.renderOption)}
                           data-name={name}
                           aria-invalid={props.invalid || props.error ? 1 : 0} />
                    {Button === undefined &&
                    <>
                    {isEmpty || notClearable ?
                            <Icon type="chevron-down" size={10}
                                    color="#7B8E9B"
                                     className={"icon-select-open"}
                                     />
                    :
                        <button className="react-select-item-clear show" onClick={this.onClear} />
                    }
                    </>}
                </Div>
            </Wrapper>
        )
    }
}

const Wrapper = css(Field)( {
    ".react-select-item-clear": {
        display: "none",
    },
    ".react-select-item-clear.show": {
        display: "inline-block",
    },

    ".react-select-item-search": {
        ...inputStyles,
        ...inputPropsStyles({}),
        marginLeft: "-10px",
        borderColor: "transparent !important",
        backgroundColor: "transparent !important",
    },
    ".select-item-no-results": {
        display: "none",
    },
})

const StyledSelect = css(SelectItem)(styles, stylesProps)

export default InputContainer(Select) as typeof Select
