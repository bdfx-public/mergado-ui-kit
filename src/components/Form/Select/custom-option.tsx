
import * as React from "react"
import { IOptionProps, Option as SelectOption } from "react-select-item"
import {Option} from "./"
type Props = Option & IOptionProps

export class CustomOption extends SelectOption<Props, {}> {

    printItem = (option) =>  option.text ?? option.label

    public render() {
        const t:any = this
        const {renderItem, printItem, ...option} = t.props.option
        const optionProps = t.getOptionProps()
        if(renderItem) {
            return renderItem({ ...optionProps,...option, className: `react-select-item-option ${option.className || ""} ${optionProps.className || ""}` })
        }
        return (
            <div {...optionProps} {...option} className={`react-select-item-option ${option.className || ""} ${optionProps.className || ""}`}>
                {printItem ? printItem(option) : this.printItem(option)}
            </div>
        );
    }
}
