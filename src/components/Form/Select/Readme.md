Example

    <Select
            options={[
                { label: 'Empty select', value: null}
            ]} placeholder={"Empty select"} />

    <Select button={<Button>Click me</Button>} name="form.select" options={[{ label: 'First', value: 'first'}, { label: 'Second', value: 'second'}, { label: 'Third', value: 'third'} ]} />

    <Select name="form.select" value={"second"}  printItem={(option) => option.value + "!"} invalid={true} options={[{ label: 'First', value: 'first'}, { label: 'Second', value: 'second'}, { label: 'Third', value: 'third'} ]} />

    <Select name="form.select" value={"second"} printItem={(option) => <em>{option.name}</em>}
                options={[
                { label: 'First', value: 'first1'},
                { label: 'Second', value: 'second1'},
                { label: 'Third', value: 'third1'},
                { label: 'First', value: 'first2'},
                { label: 'Second', value: 'second2'},
                { label: 'First', value: 'first1'},
                { label: 'Second', value: 'second1'},
                { label: 'Third', value: 'third1'},
                { label: 'First', value: 'first2'},
                { label: 'Second', value: 'second2'},
                { label: 'First', value: 'first1'},
                { label: 'Second', value: 'second1'},
                { label: 'Third', value: 'third1'},
                { label: 'First', value: 'first2'},
                { label: 'Second', value: 'second2'},                
                { label: 'First', value: 'first1'},
                { label: 'Second', value: 'second1'},
                { label: 'Third', value: 'third1'},
                { label: 'First', value: 'first2'},
                { label: 'Second', value: 'second2'},
                    { label: 'First', value: 'first1'},
                { label: 'Second', value: 'second1'},
                { label: 'Third', value: 'third1'},
                { label: 'First', value: 'first2'},
                { label: 'Second', value: 'second2'},             
    ]} />

Multi select

    <Select name="form.multiselecct" multiple={true} options={[{ label: 'First', value: 'first'}, { label: 'Second', value: 'second'}, { label: 'Third', value: 'third'} ]} />

Uncontrolled

    <Select value={"first"} onChange={e => alert(e)} options={[{ label: 'First', value: 'first'}, { label: 'Second', value: 'second'}, { label: 'Third', value: 'third'} ]} />

Uncontrolled multi select

    <Select value={null} onChange={e => alert(e)} multiple={true} options={[{ label: 'First', value: 'first'}, { label: 'Second', value: 'second'}, { label: 'Third', value: 'third'} ]} />

    <Select value={['first','third']} onChange={e => alert(e)} multiple={true} options={[{ label: 'First', value: 'first'}, { label: 'Second', value: 'second'}, { label: 'Third', value: 'third'} ]} />
