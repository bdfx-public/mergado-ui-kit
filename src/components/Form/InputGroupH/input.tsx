import * as React from "react"
import css from "../../../css"
import Button from "../../Button"
import Span from "../../Span"
import Theme from "../../Theme"
import {Input as StyledCheckbox} from "../Checkbox/input"

interface IInputProps {
    value: string | number | boolean
    checked: boolean
    label: string | JSX.Element
    onChange: (evt: any) => void
    className?: string
    hideInput?: boolean
    singleChoice?: boolean
    name?: string
}

const RadioInput: React.SFC<IInputProps> = ({ value, checked, label, singleChoice,
                                             onChange, hideInput, name, ...props}) => {


    return <CssBigLabel className={`muk-radio ${props.className || ""}`} key={value}>
                <CssElement
                    checked={checked}
                    value={value}
                    data-name={name}
                    onChange={onChange}
                    type={singleChoice ?  "radio" : "checkbox"}
                    className={`m-input`}
                    />
                <Button
                    secondary={true}
                    className={"m-button"}
                    type="void">
                    {!hideInput &&
                        <Span className="m-input-wrapper" display={"inline-block"} verticalAlign={"middle"} height={"18px"} position={"relative"}>
                            {!singleChoice ?
                            <>
                                <StyledCheckbox checked={checked} readOnly={true} className={"m-checkbox-input"} label={label} />
                            </>
                            :
                                <CssRadio className="m-radio-input" />
                            }
                        </Span>
                    }
                    <Span className="m-label-wrapper" verticalAlign={"middle"} color={Theme.blue}>
                        {label}
                    </Span>
               </Button>
               </CssBigLabel>
}

const CssBigLabel = css("label")({
    verticalAlign: "top",
    " .muk-popuphint": {
        lineHeight: "16px",
    },
    " .m-button": {
        borderRadius: 0,
        margin: "0 0 0 -1px",
        lineHeight: "16px",
        padding: "16px",
        display: "block",
        borderColor: Theme.decoration + "!important",
        borderRight: "none",
    },
    " .muk-checkbox-input > .muk-icon .m-image": {
        verticalAlign: "initial",
    },
    " .m-button:last-of-type": {
        borderRight: "1px solid " + Theme.decoration,
    },
    " .m-label-wrapper": {
        fontWeight: "normal",
    },

})

const CssElement = css("input")({
        display: "none",
        "&:checked + .m-button .m-radio-input": {
            border: `6px solid ` + Theme.blue,
            background: "white",
        },
        // "&:checked + .m-button .m-checkbox-input": {
        //     border: `1px solid white`,
        // },
        "&:checked + .m-button": {
            background: Theme.selected_background,
        },
        // "&:checked + .m-button *": {
        //     color: "white !importat",
        //     fill: "white !important",
        // },
        // "&:checked + .m-button .muk-icon--check": {
        //     display: "inline-block",
        // },
})

const CssRadio = css("span")({
    marginRight: "5px",
    display: "inline-block",
    background: "transparent",
    width: "18px",
    height: "18px",
    position: "relative",
    verticalAlign: "middle",
    transition: "border-color 0.2s",
    borderRadius: "100%",
    ":focus": {
        outline: "none",
    },
    ":active": {
        outline: "none",
    },
    border: `1px solid ${Theme.decoration}`,
})

export default RadioInput
