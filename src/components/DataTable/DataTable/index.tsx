import * as React from "react"
import css from "../../../css"
import Div from "../../Div"
import TextInput from "../../Form/TextInput"
import Checkbox from "../../Form/Checkbox"
import { ID, Action, Filter } from "../../../helpers/types"
import Grid from "../../Grid"
import GridCell from "../../GridCell"
import uniqueId from "../../../helpers/unique_id"

interface Props {
    bulkActions?: Action[]
    filters?: Filter[]
    buttons?: JSX.Element[]
    style?: any
    className?: string
    labels?: {
        actionsBar: string
    }
    onRowSelected?: Function
}

interface State {
    selectedAll: boolean
    selectedRows: ID[]
}

export interface Context {
    actions?: Action[]
    handleSelectAll?: () => void
    selectedAll?: boolean
    selectedRows?: any[]
}

export const BulkContext = React.createContext({} as Context);


class DataTable extends React.PureComponent<Props, State> {
    readonly name = "muk-datatable"
    readonly uniqueClass = 'm-table-' + uniqueId()

    state = {
        selectedAll: false,
        selectedRows: [],
    }

    componentDidUpdate(prevProps, prevState) {
        if(this.state.selectedRows.length !== prevState.selectedRows.length && this.props.onRowSelected !== undefined ) {
            this.props.onRowSelected(this.state.selectedRows.length)
        }
    }

    handleSelectAll = () => {
        if(this.state.selectedAll) {
            this.setState({
                selectedRows: [],
                selectedAll: false,
            })
        } else {
            const selected = []
            const selector: any = document.querySelectorAll(`.${this.uniqueClass} input.m-bulk-action-item`)
            for ( const checkbox of selector) {
                const item: any = checkbox
                selected.push(parseInt(item.getAttribute("data-id"), 10))
            }
            this.setState({
                selectedRows: selected,
                selectedAll: true,
            })
        }
    }

    handleSelectRow = (id) => {
        const selected = [...this.state.selectedRows]
        const index = selected.indexOf(id)

        index === -1 ? selected.push(id) : selected.splice(index,1)

        this.setState({
            selectedRows: selected,
        })
    }

    protected renderFiltersBar() {
        return (
            <Div key={"filtersbar"} className={`m-filters`} marginBottom={"10px"} verticalAlign={"middle"} >
                <Grid cols="auto auto">
                    <GridCell>
                        {this.renderFilters()}
                    </GridCell>
                    <GridCell>
                        {this.props.buttons && this.renderButtons()}
                    </GridCell>
                </Grid>
            </Div>
        )
    }

    protected renderButtons() {
        return <Div key="button" className="m-buttons" textAlign="right" verticalAlign="bottom">
            {this.props.buttons.map(obj => obj)}
        </Div>
    }

    protected renderFilters() {
        return this.props.filters.map(obj => {
            switch(obj.type) {
                case "text":
                    return (<CssMTextFilter
                                className="m-text-filter"
                                type="search"
                                value={obj.value}
                                onChange={obj.action}
                                placeholder={obj.label}
                                key={'textfilter_'+obj.label}
                            />)
                case "checkbox":
                    return (<CssMCheckboxFilter
                                className="m-checkbox-filter"
                                onChange={obj.action}
                                checked={obj.value}
                                label={obj.label}
                                key={'checkbox_'+obj.label}
                            />)
            }
        })
    }

    public render() {
        const { className, filters, bulkActions, buttons, labels, children, ...props } = this.props
        const { selectedAll, selectedRows } = this.state
        return (
            <Div className={`${this.name} ${className || ""}`}>
                <Div className="m-filters-wrapper" whiteSpace={"nowrap"}>
                    {filters && filters.length > 0 && this.renderFiltersBar()}
                </Div>
                <Div className={"m-table-wrapper"}>
                <CssTable className={`m-table ${this.uniqueClass}`} {...props}>
                {bulkActions && bulkActions.length > 0 ?
                    <BulkContext.Provider value={{
                                actions: bulkActions,
                                labels,
                                selectedAll,
                                selectedRows,
                                handleSelectAll: this.handleSelectAll,
                                handleSelectRow: this.handleSelectRow,
                            } as Context}>
                        {children}
                    </BulkContext.Provider>
                :
                    children
                }
                </CssTable>
                </Div>
            </Div>
        )
    }
}

const CssTable = css("table")({
    width: "100%",
    " .sortable-ghost": {
      opacity: 0.1,
    },
    borderBottom: "3px solid #333",
})

const CssMTextFilter = css(TextInput)({
    paddingRight: "20px",
    display: "inline-block",
    width: "70%",
    marginBottom: 0,
})

const CssMCheckboxFilter = css(Checkbox)({
    whiteSpace: "nowrap",
    display: "inline-block",
})

export default DataTable
