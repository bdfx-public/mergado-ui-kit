import * as React from "react"
import Div from "../../Div"
import css from "../../../css"
import DataCell from "../DataCell"
import Checkbox from "../../Form/Checkbox"
import { ID, Action } from "../../../helpers/types"
import Button from "../../Button"
import Theme from "../../Theme"
import {BulkContext} from "../DataTable"

export interface Props {
    style?: any
    className?: string

    bulkActionsLabel?: string | JSX.Element
    children?: any
}

export interface State {
}

interface Context {
    actions?: Action[]
    handleSelectAll?: () => void
    selectedAll?: boolean
    selectedRows?: any[]
}

export const HeaderContext = React.createContext({} as { header:  true });


class DataHeader extends React.PureComponent<Props, State, Context> {
    static contextType = BulkContext;

    readonly name = "muk-datarow";

    renderBulkActions = () => this.context.actions.map(obj => (
                    <Button className="m-bulk"
                            onClick={obj.action}
                            key={obj.type}
                            icon={obj.icon}
                            color="blue"
                            style={{margin: "0 2px"}}
                            size="tiny" />
    ))

    public render() {
        const { className, style,children } = this.props
        const { actions, selectedRows, handleSelectAll, selectedAll, } = this.context
        const kids = !Array.isArray(children) ? [children] : [...children]
        const lastKid = kids.pop()
        return (
            <thead className={`${this.name} ${className || ""}`}>
                <CssHeader className="m-row" selected={selectedRows && selectedRows.length > 0} style={style}>
                    <HeaderContext.Provider value={{ header: true }}>
                        {actions && actions.length > 0 &&
                            <DataCell type="header" style={{width: "1%"}}>
                                <Checkbox
                                    onChange={handleSelectAll}
                                    checked={selectedAll}
                                />
                            </DataCell>
                        }
                        {kids}
                        {selectedRows && selectedRows.length > 0 ?
                            <DataCell type="header">
                                <Div className={`m-actions`} textAlign={"center"} verticalAlign={"bottom"}>
                                        {this.props.bulkActionsLabel}
                                        <CssActionsIcons className={`m-icons`}>
                                            {this.renderBulkActions()}
                                        </CssActionsIcons>
                                </Div>
                            </DataCell>
                            :
                            lastKid}
                    </HeaderContext.Provider>
                </CssHeader>
            </thead>
        )
    }
}

export const CssActionsIcons = css("div")({
    marginTop: "-3px",
    whiteSpace: "nowrap",
})

const CssHeader = css("tr")(
(props: any) => {
    return {
        background: props.selected ? Theme.blue : "#333",
    }})


export default DataHeader
