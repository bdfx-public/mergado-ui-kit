Example test

    <Section>
    <DataTable onRowSelected={(e) => console.log(e)}
                buttons={[ <span>Hello</span> ]}
                labels={{actionsBar: "Actions:"}}
                bulkActions={[
                    {icon: <Icon type="play" />, type: "play", action: () => { console.log('hello play')}},
                    {icon: <Icon type="pause" />, type: "pause", action: () => { console.log('hello pause')}},
                    {icon: <Icon type="trash" />, type: "trash", action: () => { console.log('hello world')}},
                ]}
                filters={[
                    { type: "text", label: "Name:", action: (evt) => {  console.log(evt)  }},
                    { type: "checkbox", label: "Active only", action: (evt) => {  console.log(evt)  }},
                ]}
    >
        <DataHeader key="dataheader">
            <DataCell sticky="top" key={1}>Priorita</DataCell>
            <DataCell sticky="top" key={2}/>
            <DataCell sticky="top" key={3}>Názov šablóny</DataCell>
            <DataCell sticky="top" key={4}>Formát</DataCell>
            <DataCell sticky="top" key={5}>Dátum</DataCell>
            <DataCell sticky="top" key={6}>Akcia</DataCell>
        </DataHeader>
        <DataBody  key="databody"
         sortable={true}
         sortableProps={{ options: { handle: '.muk-icon--arrows-v' } }}>
            <DataRow dataId={1} key={1}>
                <DataCell key={1} style={{textAlign: 'center'}}><Icon style={{cursor: 'move'}} type="arrows-v" /></DataCell>
                <DataCell key={2} style={{textAlign: 'center'}}>1</DataCell>
                <DataCell key={3}><a href="#">[Ukázková šablona] Akce</a></DataCell>
                <DataCell key={4}>1200x628</DataCell>
                <DataCell key={5}>8. 3. 2017</DataCell>
                <DataCell key={6}>

                </DataCell>
            </DataRow>
            <DataRow dataId={1} key={2} inactive={true}>
                <DataCell key={1} style={{textAlign: 'center'}}><Icon style={{cursor: 'move'}} type="arrows-v" /></DataCell>
                <DataCell key={2} style={{textAlign: 'center'}}>2</DataCell>
                <DataCell key={3}><a href="#">[Ukázková šablona] Skladem</a></DataCell>
                <DataCell key={4}>600x600</DataCell>
                <DataCell key={5}>8. 3. 2017</DataCell>
                <DataCell key={6}>

                </DataCell>
            </DataRow>
            <DataRow dataId={3} key={3}>
                <DataCell key={1} style={{textAlign: 'center'}}><Icon style={{cursor: 'move'}} type="arrows-v" /></DataCell>
                <DataCell key={2} style={{textAlign: 'center'}}>3</DataCell>
                <DataCell key={3}><a href="#">[Ukázková šablona] Doprava zdarma</a></DataCell>
                <DataCell key={4}>1200x628</DataCell>
                <DataCell key={5}>18. 3. 2017</DataCell>
                <DataCell key={6} style={{width: "1%", whiteSpace: "nowrap"}}>

                </DataCell>
            </DataRow>
        </DataBody>
    </DataTable>

    <br/>
    without bulk actions and filters:
    <DataTable>
        <DataHeader key="dataheader">
            <DataCell key={1}>Priorita</DataCell>
            <DataCell key={2}/>
            <DataCell key={3}>Názov šablóny</DataCell>
            <DataCell key={4}>Formát</DataCell>
            <DataCell key={5}>Dátum</DataCell>
        </DataHeader>
        <DataBody  key="databody"
         sortable={true}
         sortableProps={{ options: { handle: '.muk-icon--arrows-v' } }}>
            <DataRow dataId={1} key={1}>
                <DataCell key={1} style={{textAlign: 'center'}}><Icon style={{cursor: 'move'}} type="arrows-v" /></DataCell>
                <DataCell key={2} style={{textAlign: 'center'}}>1</DataCell>
                <DataCell key={3}><a href="#">[Ukázková šablona] Akce</a></DataCell>
                <DataCell key={4}>1200x628</DataCell>
                <DataCell key={5}>8. 3. 2017</DataCell>
            </DataRow>
            <DataRow dataId={2} key={2} inactive={true}>
                <DataCell key={1} style={{textAlign: 'center'}}><Icon style={{cursor: 'move'}} type="arrows-v" /></DataCell>
                <DataCell key={2} style={{textAlign: 'center'}}>2</DataCell>
                <DataCell key={3}><a href="#">[Ukázková šablona] Skladem</a></DataCell>
                <DataCell key={4}>600x600</DataCell>
                <DataCell key={5}>8. 3. 2017</DataCell>
            </DataRow>
            <DataRow dataId={3} key={3}>
                <DataCell key={1} style={{textAlign: 'center'}}><Icon style={{cursor: 'move'}} type="arrows-v" /></DataCell>
                <DataCell key={2} style={{textAlign: 'center'}}>3</DataCell>
                <DataCell key={3}><a href="#">[Ukázková šablona] Doprava zdarma</a></DataCell>
                <DataCell key={4}>1200x628</DataCell>
                <DataCell key={5}>18. 3. 2017</DataCell>
            </DataRow>
        </DataBody>
    </DataTable>
    </Section>

Example 2

    <Section>
    <DataTable onRowSelected={(e) => console.log(e)}
                buttons={[ <span>Hello</span> ]}
                labels={{actionsBar: "Actions:"}}
                style={{ borderBottom: "5px solid black", }}
                bulkActions={[
                    {icon: <Icon type="play" />, type: "play", action: () => { console.log('hello play')}},
                    {icon: <Icon type="pause" />, type: "pause", action: () => { console.log('hello pause')}},
                    {icon: <Icon type="trash" />, type: "trash", action: () => { console.log('hello world')}},
                ]}
                filters={[
                    { type: "text", label: "Name:", action: (evt) => {  console.log(evt)  }},
                    { type: "checkbox", label: "Active only", action: (evt) => {  console.log(evt)  }},
                ]}
    >
        <DataHeader key="dataheader">
            <DataCell key={1} type='header'>Priorita</DataCell>
            <DataCell key={2} type='header'/>
            <DataCell key={3} type='header'>Názov šablóny</DataCell>
            <DataCell key={4} type='header'>Formát</DataCell>
            <DataCell key={5} type='header'>Dátum</DataCell>
            <DataCell key={6} type='header'>Akcia</DataCell>
        </DataHeader>
        <DataBody  key="databody"
         sortable={true}
         sortableProps={{ options: { handle: '.muk-icon--arrows-v' } }}>
            <DataRow dataId={1} key={1}>
                <DataCell key={1} style={{textAlign: 'center'}}><Icon style={{cursor: 'move'}} type="arrows-v" /></DataCell>
                <DataCell key={2} style={{textAlign: 'center'}}>1</DataCell>
                <DataCell key={3}><a href="#">[Ukázková šablona] Akce</a></DataCell>
                <DataCell key={4}>1200x628</DataCell>
                <DataCell key={5}>8. 3. 2017</DataCell>
                <DataCell key={6}>

                </DataCell>
            </DataRow>
            <DataRow dataId={2} key={2} inactive={true}>
                <DataCell key={1} style={{textAlign: 'center'}}><Icon style={{cursor: 'move'}} type="arrows-v" /></DataCell>
                <DataCell key={2} style={{textAlign: 'center'}}>2</DataCell>
                <DataCell key={3}><a href="#">[Ukázková šablona] Skladem</a></DataCell>
                <DataCell key={4}>600x600</DataCell>
                <DataCell key={5}>8. 3. 2017</DataCell>
                <DataCell key={6}>

                </DataCell>
            </DataRow>
            <DataRow dataId={3} key={3}>
                <DataCell key={1} style={{textAlign: 'center'}}><Icon style={{cursor: 'move'}} type="arrows-v" /></DataCell>
                <DataCell key={2} style={{textAlign: 'center'}}>3</DataCell>
                <DataCell key={3}><a href="#">[Ukázková šablona] Doprava zdarma</a></DataCell>
                <DataCell key={4}>1200x628</DataCell>
                <DataCell key={5}>18. 3. 2017</DataCell>
                <DataCell key={6} style={{width: "1%", whiteSpace: "nowrap"}}>

                </DataCell>
            </DataRow>
        </DataBody>
    </DataTable>
    </Section>

