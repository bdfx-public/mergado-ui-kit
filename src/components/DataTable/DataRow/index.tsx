import * as React from "react"
import css from "../../../css"
import DataCell from "../DataCell"
import Checkbox from "../../Form/Checkbox"
import { ID, } from "../../../helpers/types"
import Theme from "../../Theme"
import {BulkContext} from "../DataTable"

export interface Props {
    dataId?: ID
    style?: any
    className?: string
    inactive?: boolean
    /** array of bulk actions - more info in DataTable component */
    // actions?: Action[]
    /** needed for bulk actions */
    // selectedAll?: boolean
    /** needed for bulk actions */
    // handleSelectRow?: (id: ID) => {}
    // /** needed for bulk actions */
    // selectedRows?: ID[]
}

export interface State {
}

class DataRow extends React.PureComponent<Props, State> {
  static contextType = BulkContext;

    // public static defaultProps: Props = {
    //     className: "",
    //     style: {},
    //     inactive: false,
    //     dataId: "",
    //     // actions: [],
    //     // selectedRows: [],
    // }
    private readonly name = "muk-datarow"

    handleSelectRow = evt => {
        return this.context.handleSelectRow ? this.context.handleSelectRow(this.props.dataId) : false
    }

    public render() {
        const { style, inactive, dataId, className } = this.props
        const { selectedRows, actions, } = this.context
        const isSelected = selectedRows ? selectedRows.indexOf(dataId) > -1 : false
        return (
            <CssTr className={`${this.name} ${!!inactive && `inactive`} ${className ? className : ""}`}
                disabled={inactive}
                selected={isSelected}
                data-id={dataId} style={style}>
                    {actions && actions.length > 0 &&
                        <DataCell className="m-actions-cell">
                            <Checkbox
                                className="m-bulk-action-item"
                                onChange={this.handleSelectRow}
                                checked={selectedRows && selectedRows.indexOf(dataId) !== -1}
                                dataId={dataId}
                            />
                        </DataCell>
                    }
                    {this.props.children}
            </CssTr>
        )
    }
}

const CssTr = css("tr")({
},(props: any) => {
    return {
        background: props.selected ? Theme.selected_background : "#fff",
        ":hover td": {
            background: Theme.hover_background,
        },
        color: props.disabled ? "#ccc" : "initial",
        " *, path": {
            color: props.disabled && "#ccc !important",
            fill: props.disabled && "#ccc !important",
        },
        " .muk-icon--pause *": {
            color: Theme.blue + "!important",
            fill: Theme.blue + "!important",
        },
        "& .muk-icon--play *": {
            color: Theme.blue + "!important",
            fill: Theme.blue + "!important",
        },
    }
})

export default DataRow
