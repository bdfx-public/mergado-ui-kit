import * as React from "react"
import * as Sortable from "react-sortablejs"
import css from "../../../css"

export interface Props {
    style?: any
    addClass?: string
    className?: string
    sortable?: boolean
    sortableProps?: any
}

export interface State {
}

class DataBody extends React.PureComponent<Props, State> {

    private readonly name = "muk-databody";

    public render() {
        const { sortable, sortableProps, style, className } = this.props;
        if(sortable) {
            return (
                <Sortable className={`${this.name} m-sortable ${className || ""}`} style={style} tag="tbody" {...sortableProps}>
                    {this.props.children}
                </Sortable>
            )

        } else {
            return (
                <TBody className={`${this.name} ${className || ""}`} style={style}>
                    {this.props.children}
                </TBody>
            )
        }
    }
}

const TBody = css("tbody")({
})

export default DataBody
