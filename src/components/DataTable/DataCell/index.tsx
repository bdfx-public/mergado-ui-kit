import * as React from "react"
import css from "../../../css"
import Theme from "../../Theme"
import {HeaderContext} from "../DataHeader"

export interface Props {
    style?: any
    className?: string
    type?: "cell" | "header"
    onClick?: (event: any) => any
    sticky?: "top" | "bottom" | "left" | "right",
    left?: number | string,
    right?: number | string,
    bottom?: number | string,
    top?: number | string,
    align?: string,
    valign?: string,
    colSpan?: number | string
    rowSpan?: number | string
    width?: number | string
    height?: number | string
    dangerouslySetInnerHTML?: any
}
export interface State {
}

class DataCell extends React.PureComponent<Props, State> {
    private readonly name = "muk-datacell"
    static contextType = HeaderContext

    public render() {
        const { type, onClick, className, ...props } = this.props

        return (type === "header" || this.context.header)
            ?
                <CssTh {...props} className={`${this.name} m-header ${className || ""}`}
                   onClick={onClick}>{this.props.children}</CssTh>
            :
                <CssTd {...props} className={`${this.name} ${className || ""}`}
                    onClick={onClick}>{this.props.children}</CssTd>
    }
}

const Cell = {
    fontWeight: "normal",
    "& .muk-form__group": {
        margin: 0,
    },
    "& .muk-form__label": {
        padding: 0,
    },
    "& .muk-button__item": {
        padding: "0 5px",
        verticalAlign: "middle",
        height: "auto",
    },
}

const CssTd = css("td")({...Cell,...{
    padding: Theme.table_cell_padding,
    borderTop: Theme.table_border_horizontal,
    borderRight: Theme.table_border_vertical,
    fontSize: Theme.table_cell_text_size,
    " a, a:visited, a:hover, a:active": {
        // color: Theme.text,
    },
    "&:first-of-type": {
        borderLeft: "1px solid " + Theme.decoration,
    },
    "&:last-of-type": {
        borderRight: "1px solid " + Theme.decoration,
    },}}, props => ({
        ...props.sticky && { position: "sticky" },
        ...props.left && { left: props.left },
        ...props.right && { right: props.right },
        ...props.top && { right: props.top },
        ...props.bottom && { right: props.bottom },
        textAlign: props.align || "left",
        verticalAlign: props.valign || undefined,
    }))

const CssTh = css("th")({
    ...Cell,
    borderColor: "transparent",
    whiteSpace: "nowrap",
    color: "#fff",
    fontSize: Theme.table_header_text_size,
    textTransform: Theme.table_header_text_transform,
    fontWeight: "bold",
    padding: "5px 8px 2px",
    " .muk-checkbox": {
        marginBottom: "-3px",
    },
}, props => ({
        ...props.sticky && { position: "sticky", background: "inherit", boxShadow: `${props.sticky === "left" ? "2px" : props.sticky === "right" ? "-2px" : "0px"} ${props.sticky === "top" ? "2px" : props.sticky === "bottom" ? "-2px" : "4px"} 5px 0 rgba(0,0,0,0.05)`  },
        ...(props.left !== undefined || props.sticky === "left") && { left: props.left || 0 },
        ...(props.right !== undefined || props.sticky === "right") && { right: props.right || 0 },
        ...(props.top !== undefined || props.sticky === "top") && { top: props.top || 0 },
        ...(props.bottom !== undefined || props.sticky === "bottom") && { bottom: props.bottom || 0 },
        textAlign: props.align || "left",
        verticalAlign: props.valign || undefined,
    }))


export default DataCell
