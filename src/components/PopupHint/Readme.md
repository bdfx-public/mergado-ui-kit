Example

    <span>
        I need hint <PopupHint bottom={true} icon={<Button size="smaller" icon="arrow-left" />}>
            Zpět<br/>
            Zpět<br/>
            Zpět<br/>
            Zpět<br/>
            Zpět<br/>
        </PopupHint><br/>

        Hint with custom icon <PopupHint bottom={true} icon={<Icon type="trash" />}>
            I am hint
        </PopupHint>
    </span>