import * as React from "react"
import Div from "../Div"
import Icon from "../Icon"
import Bubble from "./Bubble"

export interface Props {
    icon?: JSX.Element | string | number
    style?: any
    hint?: boolean
    help?: boolean
    bottom?: boolean
    className?: string
    hover?: boolean
    arrowRight?: number
    arrowLeft?: number
    size?: number
}

export interface State {
}

export interface Position {
    top: number
    width?: number
    left: number
    height?: number
}

/* </style> */

class PopupHint extends React.PureComponent<Props, State> {

    private readonly name = "muk-popuphint";
    private id:number = null
    public static defaultProps: Props = {
        icon: null,
        style: {},
        hint: false,
        help: false,
        hover: true,
        arrowLeft: 0,
        arrowRight: 0,
    }

    constructor(props: Props) {
        super(props)

        this.collapse = this.collapse.bind(this)
        this.expand = this.expand.bind(this)
        this.styleHint = this.styleHint.bind(this)
        this.id = Math.round(Math.random() * 1000)
    }

    protected expand(event: any) {
        event.preventDefault()
        const hint: any = document.getElementById('muk-popuphint')
        hint.classList.add("m-active")
        const content = document.getElementById("muk-popup-content-"+this.id).innerHTML
        document.getElementById('muk-popup-bubble').innerHTML = content
        this.styleElements()
    }

    protected doNothing() {
    }

    protected collapse(): void {
        const hint: any = document.getElementById('muk-popuphint')
        hint.classList.remove("m-active")
    }

    protected getWindowWidth(): number {
        if (window.innerWidth) {
            return window.innerWidth;
        }

        if (document.documentElement && document.documentElement.clientWidth) {
            return document.documentElement.clientWidth;
        }

        if (document.body) {
            return document.body.clientWidth;
        }

        return 0
    }

    protected styleElements() {
        const buttonPosition = this.getPosition(this.refs.button)
        const windowWidth = this.getWindowWidth()
        const widthLeft = buttonPosition.left
        const renderLeft: boolean = widthLeft > windowWidth / 2
        this.styleHint(buttonPosition, renderLeft)
        this.styleHint(buttonPosition, renderLeft) // this is not error, repeat, this is not error
    }

    protected styleHint(buttonPosition: Position, renderLeft: boolean) {
        const hint: any = document.getElementById('muk-bubble')
        hint.style.display = "block"
        hint.style.pointerEvents = "none"
        let newX: number
        if (renderLeft) {
            newX = buttonPosition.left - hint.offsetWidth + 14

        } else {
            newX = (buttonPosition.left - 2) > 0 ? (buttonPosition.left + (buttonPosition.width / 2) -  (hint.offsetWidth / 2)) : 0;
        }
        if (hint.style.top === `${this.props.bottom ? (buttonPosition.top + buttonPosition.height) : (buttonPosition.top - hint.offsetHeight)}px` &&
            hint.style.left === `${newX}px`) {
        } else {
            hint.style.top = `${this.props.bottom ? (buttonPosition.top + buttonPosition.height) : (buttonPosition.top - hint.offsetHeight)}px`
            hint.style.left = `${newX}px`
        }
        if(!this.props.hover) {
            const toggler: any = document.getElementById('muk-popup-toggler-'+this.id)
            toggler.focus()
        }
    }

    protected getPosition(element: any): any {
        let top = 10
        let left = 0
        let width = element.offsetWidth
        let height = element.offsetHeight
        do {
            top += element.offsetTop || 0
            left += element.offsetLeft || 0
            element = element.offsetParent
        } while (element)

        return {
            top,
            left,
            width,
            height,
        }
    }

    public render() {
        const { children, className, hover, icon, style, hint, help, arrowLeft, arrowRight, ...p } = this.props

        return (
            <Div className={`${this.name} ${className || ""}`}
                props={{
                    id: `muk-popup-toggler-${this.id}`,
                    onMouseEnter: hover ? this.expand : this.doNothing,
                    onMouseLeave: hover ? this.collapse : this.doNothing,
                    onMouseDown: hover ? this.doNothing : this.expand,
                    onBlur: hover ? this.doNothing : this.collapse,
                    tabIndex: 0,
                    ...p,
                }}
                display="inline-block"
                {...style}>
                    <Div props={{ id: `muk-popup-content-${this.id}` }} display={"none"}>
                        {children}
                    </Div>
                    <div ref="button" className={`m-trigger`} style={{position: "relative", top: "-1px"}}>
                        {hint ? <Icon type="hint-info" className="m-icon" color="#ccc" size={this.props.size || 14} /> : null}
                        {help ? <Icon type="hint-help" className="m-icon" color="#ccc" size={this.props.size || 14} /> : null}
                        {icon ? icon : null }
                    </div>
                    <Bubble />
            </Div>
        );
    }
}

export default PopupHint
