Import icon this way:

    <code>import Icon from "@bdfx/mergado-ui-kit"</code>

And then use it like this:  `<Icon type="close" size="20" />`
