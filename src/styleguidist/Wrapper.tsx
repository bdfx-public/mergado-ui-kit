import * as React from 'react';
import { Provider } from "react-redux";
import { createStore, combineReducers } from 'redux';
import formReducer from '../formReducer';

const rootReducer = combineReducers({
    form: formReducer.Reducer('form'),
});
const w:any = window
const store = createStore(rootReducer, w.__REDUX_DEVTOOLS_EXTENSION__ && w.__REDUX_DEVTOOLS_EXTENSION__());

export default class Wrapper extends React.PureComponent {
    render() {
        return <Provider store={store}>
                    <div style={{position: "relative"}}>
                    {this.props.children}
                    </div>
                </Provider>
    }
}
